import matplotlib.pyplot as plt
import numpy as np
import time
import csv
import glob

#with open('../nn_baseline_log.csv') as f:

for file in sorted(glob.glob('*_test.txt')):
    try:
        with open(file) as f:
            lines = f.readlines()
        test = [float(i.replace("*","").replace('\n','').split(':')[1]) for i in lines if not "#" in i]
        x_scale = [float(i.replace("*","").replace('\n','').split(':')[0]) for i in lines if not "#" in i]
        max = [float(i.replace("*","").replace('\n','').split(':')[1]) for i in lines if "*" in i][-1]

        with open(file.replace('_test.txt', '_train.txt')) as f:
            lines = f.readlines()
        train = []
        lastx = []
        for line in range(len(lines)):
            if line % (x_scale[0]-100) == 0 and len(lastx) > 0:
                train.append(sum(lastx) / len(lastx))
                lastx = []
            if not "x" in lines[line]:
                lastx.append(float(lines[line].replace('\n','')))
        lines = ""

        #plt.ion()
        #plt.scatter(episodes, scores, alpha=0.1, c=colors)
        try:
            plt.plot(x_scale, train[:len(x_scale)], color='black', label='Train')
        except:
            pass
        plt.plot(x_scale, test, color='red', label='Test')
        plt.legend()

        print("Plotting %s (%s x %s) \t --   MAX%s , MEAN%s" % (file, len(test), x_scale[0], max, np.mean(test)))
        plt.show()
    except:
        print("No Plot for %s" % file)

import sys
sys.exit()
