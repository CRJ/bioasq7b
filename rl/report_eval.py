"""report_eval.py - Print train and test evaluation results"""
import csv
import numpy as np
import sys

LOGFILE = "../reinforce_log.csv"
EVALFILE = "../reinforce_eval.csv"
#LOGFILE = "../nn_baseline_log.csv"
#EVALFILE = "../nn_baseline_eval.csv"

with open(LOGFILE) as f:
    reader = csv.DictReader(f)
    loglines = [l for l in reader]

with open(EVALFILE) as f:
    reader = csv.DictReader(f)
    evallines = [l for l in reader]

print("| Episodes | Train ROUGE F1 | Test ROUGE F1 |")
print("|----------+----------------+---------------|")

#episodes = (1000, 5000, 10000, 20000, 50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000)
#evalstep = 1000
#episodes = (200, 400, 600, 800, 1000)
evalstep = 200
last_episode = int(evallines[-1]['episode'])

first_episode = evalstep
if len(sys.argv) > 1:
    first_episode = int(sys.argv[1])

for episode in range(first_episode, last_episode + 1, evalstep):
    lines = [float(l['reward']) for l in loglines[episode-evalstep:episode]]
    trainscore = np.mean(lines)

    lines = [float(l['reward']) for l in evallines if int(l['episode']) == episode]
    testscore = np.mean(lines)

    if (int(loglines[-1]['episode']) + 1 == last_episode) and (episode == last_episode):
        print("| %i* | %f | %f |" % (episode, trainscore, testscore))
    else:
        print("| %i | %f | %f |" % (episode, trainscore, testscore))
