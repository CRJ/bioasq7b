"""Using the REINFORCE algorithm"""
#import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import numpy as np
from nltk import sent_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import load_model
import json
import csv
import pickle

from my_tokenizer import my_tokenize
from rl import Environment, yield_candidate_text, rouge_engine, DEBUG, ROUGE

VERBOSE = 1
RESTORE = False
CHECKPOINT_PATH = "reinforce.ckpt"
NN_CHECKPOINT_PATH = "nn_baseline.ckpt"
BEST_CHECKPOINT_PATH = "best_reinforce.ckpt"
TFIDF_FILENAME = "reinforce_tfidf.pickle"
BEST_TFIDF_FILENAME = "best_reinforce_tfidf.pickle"
N_HIDDEN = 200
if DEBUG:
    SAVE_EPISODES = 20
else:
    SAVE_EPISODES = 200
LOGFILE = "reinforce_log.csv"
NN_LOGFILE = "nn_baseline_log.csv"
EVALFILE = "reinforce_eval.csv"
NN_EVALFILE = "nn_baseline_eval.csv"
NOISE = 0.2 # Noise added when computing the action for training

def yieldRouge(CorpusFile, snippets_only=True):
    """yield ROUGE scores of all sentences in corpus
    >>> rouge = yieldRouge('BioASQ-trainingDataset5b.json')
    >>> target = (0, '15829955', 0, {'N-1': 0.1519, 'S4': 0.0, 'SU4': 0.04525, 'N-2': 0.0, 'L': 0.0}, 'The identification of common variants that contribute to the genesis of human inherited disorders remains a significant challenge.')
    >>> next(rouge) == target
    True
    >>> target2 = (0, '15829955', 1, {'N-1': 0.31915, 'S4': 0.02273, 'SU4': 0.09399, 'N-2': 0.13043, 'L': 0.04445}, 'Hirschsprung disease (HSCR) is a multifactorial, non-mendelian disorder in which rare high-penetrance coding sequence mutations in the receptor tyrosine kinase RET contribute to risk in combination with mutations at other genes.')
    >>> next(rouge) == target2
    True
    """
    data = json.load(open(CorpusFile, encoding='utf-8'))['questions']
    for qi in range(len(data)):
        if 'snippets' not in data[qi].keys():
            print("Warning: No snippets in question %s" % data[qi]['body'])
            continue
        ai = 0
        if type(data[qi]['ideal_answer']) == list:
            ideal_answers = data[qi]['ideal_answer']
        else:
            ideal_answers = [data[qi]['ideal_answer']]
        for (pubmedid, senti, sent) in yield_candidate_text(data[qi], snippets_only=snippets_only):
            if ROUGE == 'PYTHON':
                rouge_scores = [rouge_engine.get_scores(h, sent)[0] for h in ideal_answers]
                rouge_value = max([(r['rouge-2']['f'] + r['rouge-l']['f'])/2 for r in rouge_scores])
            else:
                rouge_scores = rouge_engine.get_scores(ideal_answers, sent)
                rouge_score = rouge_scores['SU4']
            yield (qi, pubmedid, senti, rouge_value, sent)

def saveRouge(corpusfile, outfile, snippets_only=True):
    "Compute and save the ROUGE scores of the individual snippet sentences"
    with open(outfile,'w') as f:
        writer = csv.writer(f)
#        writer.writerow(('qid','snipid','sentid','N1','N2','L','S4','SU4','sentence text'))
        writer.writerow(('qid','pubmedid','sentid','L','sentence text'))
        for (qi,qsnipi,senti,F,sent) in yieldRouge(corpusfile, snippets_only=snippets_only):
            writer.writerow((qi,qsnipi,senti,F,sent))


class NNModel():
    def __init__(self, vocabulary_size):
        X_state = keras.layers.Input(shape=(4*vocabulary_size + 1,))
        Q_state = keras.layers.Input(shape=(vocabulary_size,))
        reward = keras.layers.Input(shape=(1,))
        all_inputs = keras.layers.Concatenate()([X_state, Q_state])
        self.episode = tf.placeholder(tf.float32)
        hidden = keras.layers.Dense(N_HIDDEN, activation="relu")(all_inputs)
        outputs = keras.layers.Dense(1, activation="sigmoid")(hidden)

        def custom_loss(y_true, y_predict):
            "From https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
            #log_lik = K.log(y_true * (y_true - y_predict) + (1 - y_true) * (y_true + y_predict))
            loss = - (y_true * K.log(y_predict) + (1 - y_true) * K.log(1 - y_predict)) # Using the standard cross-entropy formula
            return K.mean(loss * reward, keepdims=True)

        self.model_train = keras.models.Model([X_state, Q_state, reward], outputs)
        self.model_train.compile(loss=custom_loss, optimizer=keras.optimizers.Adam())

        self.model_predict = keras.models.Model([X_state, Q_state], outputs)

def baseline(testfile=EVALFILE):
    """Evaluate a baseline that returns the first n sentences"""
    nanswers = {"summary": 6,
                "factoid": 2,
                "yesno": 2,
                "list": 3}
    env = Environment(jsonfile='BioASQ-training7b.json')
    if type(testfile) == None:
        alldata = list(range(len(env.data)))
        np.random.shuffle(alldata)
        split_boundary = int(len(alldata)*.8)
        train_indices = alldata[:split_boundary]
        test_indices = alldata[split_boundary:]
    else:
        with open(testfile) as f:
            reader = csv.DictReader(f)
            test_indices = list(set(int(l['QID'].split('-')[0]) for l in reader))

    scores = []
    for x in test_indices:
        observation = env.reset(x)
        n = nanswers[env.qtype]
        if len(env.candidates) == 0:
            continue

        while not observation['done']:
            this_candidate = observation['next_candidate']
            if this_candidate < n:
                action = 1
            else:
                action = 0
            observation = env.step(action)
        reward = observation['reward']
        scores.append(reward)
    return np.mean(scores)

def train():
    with open(LOGFILE, 'w') as f:
        f.write("episode,reward,QID,summary\n")

    with open(EVALFILE, 'w') as f:
        f.write("episode,reward,QID,summary\n")

    env = Environment(jsonfile='BioASQ-training7b.json')
    alldata = list(range(len(env.data)))
    np.random.shuffle(alldata)
    split_boundary = int(len(alldata)*.8)
    train_indices = alldata[:split_boundary]
    test_indices = alldata[split_boundary:]

    # train tf.idf
    if RESTORE:
        with open(TFIDF_FILENAME,'rb') as f:
                tfidf = pickle.load(f)
    else:
        if VERBOSE > 0:
            print("Training tf.idf")
        tfidf_train_text = [env.data[x]['body'] for x in train_indices]
        tfidf_train_text += [c[2] for x in train_indices for c in yield_candidate_text(env.data[x], snippets_only=True)]
        ideal_summaries_sentences = []
        for x in train_indices:
            ideal_summaries = env.data[x]['ideal_answer']
            if type(ideal_summaries) != list:
                ideal_summaries = [ideal_summaries]
            for ideal_sum in ideal_summaries:
                ideal_summaries_sentences += sent_tokenize(ideal_sum)
        tfidf_train_text += ideal_summaries_sentences
        #print(len(tfidf_train_text))
        #print(tfidf_train_text[:10])
        tfidf = TfidfVectorizer(tokenizer=my_tokenize)
        tfidf.fit(tfidf_train_text)
        with open(TFIDF_FILENAME,'wb') as f:
            pickle.dump(tfidf,f)
    nnModel = NNModel(len(tfidf.get_feature_names()))

    if VERBOSE > 0:
        print("Training REINFORCE")

    if RESTORE:
        nnModel.model_train = load_model(CHECKPOINT_PATH)

    while True:
        train_x = np.random.choice(train_indices)
        observation = env.reset(train_x) # Reset to a random question

        if len(env.candidates) > 0:
            break
    tfidf_all_candidates = tfidf.transform(env.candidates)
    tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]

    best_test_score = 0.0
    #nnModel.model_predict.save(BEST_CHECKPOINT_PATH)
    with open(BEST_TFIDF_FILENAME,'wb') as f:
        pickle.dump(tfidf,f)

    states = []
    questions = []
    actions = []

    episode = 0
    while True:
        # The following code is based on "Policy Gradients"
        # at https://github.com/ageron/handson-ml/blob/master/16_reinforcement_learning.ipynb with adaptation to
        # Keras from  https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
        this_candidate = observation['next_candidate']
        tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
        tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0, :]
        tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0, :]
        tfidf_question = tfidf.transform([env.question]).todense()[0,:]
        #print(tfidf_question.shape)
        XState = np.hstack([tfidf_all_text,
                            tfidf_this_candidate,
                            tfidf_remaining_candidates,
                            tfidf_summary,
                            [[len(observation['summary'])]]])
        predict = nnModel.model_predict.predict([XState, tfidf_question])
        # print("Prediction:", predict)
        predict = predict[0][0]
        perturb = NOISE*3000/(3000+episode) # decrease the perturbation with each training episode
        p_left_and_right = [(predict+perturb)/(1+2*perturb), (1-predict+perturb)/(1+2*perturb)]
        action_val = np.random.choice(2, p=p_left_and_right)

        states.append(XState)
        questions.append(tfidf_question)
        actions.append(1 - action_val)

        observation = env.step(action_val)

        if observation['done']:
            # reward all actions that lead to the summary
            reward = observation['reward']
            print("Episode: %i, reward: %f" % (episode, reward))
            try:
                with open(LOGFILE, 'a') as f:
                    f.write('%i,%f,%i-%s,"%s"\n' % (episode, reward, env.qid, env.qtype,
                                                " ".join([str(x) for x in observation['summary']])))
            except:
                pass
            #s = np.vstack(states)
            #q = np.vstack(questions)
            #r = np.array([reward]*len(states))
            ##print("States:", s)
            #print("Sates shape:", s.shape)
            ##print("Questions:", q)
            #print("Questions shape:", q.shape)
            #print("Rewards:", r)
            #print("Rewards shape:", r.shape)

            loss = nnModel.model_train.train_on_batch([np.vstack(states), np.vstack(questions), np.array([reward]*len(states))],
                                                      actions)
            episode += 1
            if episode % SAVE_EPISODES == 0:
                print("Saving checkpoint in %s" % (CHECKPOINT_PATH))
                #nnModel.model_predict.save(CHECKPOINT_PATH)
                print("Testing results")
                test_results = []
                for test_x in test_indices:
                    observation = env.reset(test_x)

                    if len(env.candidates) == 0:
                        continue

                    tfidf_all_candidates = tfidf.transform(env.candidates)
                    tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]
                    while not observation['done']:
                        this_candidate = observation['next_candidate']
                        tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
                        tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0,:]
                        tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0,:]
                        tfidf_question = tfidf.transform([env.question]).todense()[0,:]
                        #print(tfidf_question.shape)
                        XState = np.hstack([tfidf_all_text,
                                            tfidf_this_candidate,
                                            tfidf_remaining_candidates,
                                            tfidf_summary,
                                            [[len(observation['summary'])]]])
                        output_val = nnModel.model_predict.predict([XState, tfidf_question])
                        action_val = 0
                        if output_val < 0.5:
                            action_val = 1
                        observation = env.step(action_val)
                    reward = observation['reward']
                    test_results.append(reward)
                    try:
                        with open(EVALFILE, 'a') as f:
                            f.write('%i,%f,%i-%s,"%s"\n' % (episode,reward,env.qid,env.qtype," ".join([str(x) for x in observation['summary']])))
                    except:
                        pass
                mean_test_results = np.mean(test_results)
                print("Mean of evaluation results:", mean_test_results)
                if mean_test_results > best_test_score:
                    print("Test results improved; saving model")
                    #nnModel.model_train.save(BEST_CHECKPOINT_PATH)
                    best_test_score = mean_test_results

            # Pick next training question
            while True:
                train_x = np.random.choice(train_indices)
                observation = env.reset(train_x) # Reset to a random question

                if len(env.candidates) > 1:
                    break

            states = []
            questions = []
            actions = []
            tfidf_all_candidates = tfidf.transform(env.candidates)
            tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]

def test(outfile=None, jsonfile=None):
    env = Environment(jsonfile='BioASQ-training7b.json')
    with open(EVALFILE) as f:
        reader = csv.DictReader(f)
        test_indices = list(set(int(l['QID'].split('-')[0]) for l in reader))
    with open(BEST_TFIDF_FILENAME, 'rb') as f:
        tfidf = pickle.load(f)
    nnModel = NNModel(len(tfidf.get_feature_names()))
    nnModel.model_predict.load_weights(BEST_CHECKPOINT_PATH)
    result_json = []
    test_results = []
    #test_indices = test_indices[:20]

    for test_x in test_indices:
        observation = env.reset(test_x)
        if len(env.candidates) == 0:
            continue

        tfidf_all_candidates = tfidf.transform(env.candidates)
        tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]
        predicted_probs = []
        while not observation['done']:
            this_candidate = observation['next_candidate']
            tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
            tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0,:]
            tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0,:]
            tfidf_question = tfidf.transform([env.question]).todense()[0,:]
            #print(tfidf_question.shape)
            XState = np.hstack([tfidf_all_text,
                                tfidf_this_candidate,
                                tfidf_remaining_candidates,
                                tfidf_summary,
                                [[len(observation['summary'])]]])
            output_val = nnModel.model_predict.predict([XState, tfidf_question])
            predicted_probs.append(output_val)
            action_val = 0
            if output_val < 0.5:
                action_val = 1
            observation = env.step(action_val)
        reward = observation['reward']
        test_results.append(reward)
        print("Reward:", reward)
        if outfile:
            with open(outfile,'a') as f:
                f.write(','.join(str(p[0][0]) for p in predicted_probs))
                f.write('\n')
        # Create information for JSON output
        if jsonfile:
            if env.qtype == "yesno":
                exactanswer = "yes"
            else:
                exactanswer = ""
            result_json.append({"id": env.id,
                                "ideal_answer": " ".join([env.candidates[x] for x in observation['summary']]),
                                "exact_answer": exactanswer})

    if jsonfile:
        print("Saving results in file %s" % jsonfile)
        with open(jsonfile, 'w') as f:
            f.write(json.dumps({"questions": result_json}, indent=2))

    print("Mean of evaluation results:", np.mean(test_results))

def bioasq_run(test_data='phaseB_5b_01.json',
               output_filename='bioasq-out-rl.json'):
    """Run model for BioASQ"""
    print("Running BioASQ")
    with open(BEST_TFIDF_FILENAME, 'rb') as f:
        tfidf = pickle.load(f)
    testset = json.load(open(test_data, encoding='utf-8'))['questions']
    if DEBUG:
        testset = testset[:10]
    result = []
    nnModel = NNModel(len(tfidf.get_feature_names()))
    nnModel.model_predict.load_weights(BEST_CHECKPOINT_PATH)
    for r in testset:
        test_question = r['body']
        test_id = r['id']
        test_candidates = [sent
                           for pubmedid, senti, sent
                           in yield_candidate_text(r)]
        test_candidates = test_candidates[:20]
        if len(test_candidates) == 0:
            print("Warning: no text to summarise")
            test_summary = ''
        elif len(test_candidates) == 1:
            # Trivial summary: return the only candidate
            test_summary = test_candidates[0]
        else:
            tfidf_all_candidates = tfidf.transform(test_candidates)
            tfidf_all_text = tfidf.transform([" ".join(test_candidates)]).todense()[0,:]
            test_summary = ''
            len_summary = 0
            output_probs = []
            for this_candidate in range(len(test_candidates)):
                tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
                tfidf_remaining_candidates = tfidf.transform([" ".join(test_candidates[this_candidate + 1:])]).todense()[0,:]
                tfidf_summary = tfidf.transform([test_summary]).todense()[0,:]
                tfidf_question = tfidf.transform([test_question]).todense()[0,:]
                XState = np.hstack([tfidf_all_text,
                                    tfidf_this_candidate,
                                    tfidf_remaining_candidates,
                                    tfidf_summary,
#                                  [[len(observation['summary']), this_candidate]]])
                                    [[len_summary]]])
                output_val = nnModel.model_predict.predict([XState, tfidf_question])
                output_probs.append(1-output_val)
                if output_val < 0.5:
                    len_summary += 1
                    if test_summary == '':
                        test_summary = test_candidates[this_candidate]
                    else:
                        test_summary += " " + test_candidates[this_candidate]
        if test_summary == '' and len(test_candidates) > 0:
            print("Warning: no summary produced; returning top sentence %i" % np.argmax(output_probs))
            #print("Output probabilities are:")
            #print(output_probs)
            test_summary = test_candidates[np.argmax(output_probs)]
        if r['type'] == "yesno":
            exactanswer = "yes"
        else:
            exactanswer = ""

        result.append({"id": test_id,
                       "ideal_answer": test_summary,
                       "exact_answer": exactanswer})
    print("Saving results in file %s" % output_filename)
    with open(output_filename, 'w') as f:
        f.write(json.dumps({"questions": result}, indent=2))


if __name__ == "__main__":
    train()
