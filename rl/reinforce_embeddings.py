"""Using the REINFORCE algorithm"""
#import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import numpy as np
from nltk import sent_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
import tensorflow as tf
from tensorflow import keras
import tensorflow.keras.backend as K
from tensorflow.keras.models import load_model
import json
import csv
import pickle

from my_tokenizer import my_tokenize
from rl import Environment, yield_candidate_text, rouge_engine, DEBUG

VERBOSE = 1
RESTORE = False
CHECKPOINT_PATH = "reinforce.ckpt"
NN_CHECKPOINT_PATH = "nn_baseline.ckpt"
BEST_CHECKPOINT_PATH = "best_reinforce.ckpt"
TFIDF_FILENAME = "reinforce_tfidf.pickle"
BEST_TFIDF_FILENAME = "best_reinforce_tfidf.pickle"
N_HIDDEN = 200
if DEBUG:
    SAVE_EPISODES = 20
else:
    SAVE_EPISODES = 200
LOGFILE = "reinforce_log.csv"
NN_LOGFILE = "nn_baseline_log.csv"
EVALFILE = "reinforce_eval.csv"
NN_EVALFILE = "nn_baseline_eval.csv"
NOISE = 0.2 # Noise added when computing the action for training

MEAN_EMBEDDINGS = True #True
import sqlite3
from scipy.sparse import csr_matrix
from multiprocessing import Pool
if MEAN_EMBEDDINGS:
    SENTENCE_LENGTH = 300
    EMBEDDINGS = 100
    #EMBEDDINGS = 100
    VECTORS = 'allMeSH_2016_%i.vectors.txt' % EMBEDDINGS
    DB = 'word2vec_%i.db' % EMBEDDINGS
    if __name__ == '__main__':
        with open(VECTORS) as v:
            VOCABULARY = int(v.readline().strip().split()[0]) + 2


if MEAN_EMBEDDINGS:
    def embeddings_one_sentence(row):
        return [float(x) for x in row[1].split()]
    if __name__ == '__main__':
        vectordb = sqlite3.connect(DB)
        print("Database %s opened" % DB)
        c = vectordb.cursor()
        c_iterator = c.execute("""SELECT word, data FROM vectors""")
        print("Loading word embeddings")
        with Pool() as pool:
            embeddings_list = pool.map(embeddings_one_sentence,
                                       c_iterator)
        print("Word embeddings loaded")
        vectordb.close()

def one_sentence_to_ids(sentence, sentence_length=SENTENCE_LENGTH):
    """Convert one sentence to a list of word IDs."

Crop or pad to 0 the sentences to ensure equal length if necessary.
Words without ID are assigned ID 1.
>>> one_sentence_to_ids(['my','first','sentence'], 2)
([11095, 121], 2)
>>> one_sentence_to_ids(['my','ssecond','sentence'], 2)
([11095, 1], 2)
>>> one_sentence_to_ids(['yes'], 2)
([21402, 0], 1)
"""
    vectordb = sqlite3.connect(DB)
    c = vectordb.cursor()
    word_ids = []
    for w in sentence:
        if len(word_ids) >= sentence_length:
            break
        c.execute("""SELECT rowid, word
                  FROM vectors
                  WHERE word=?""", (w, ))
        #c.execute("""SELECT word_index, word
        #          FROM vectors
        #          INDEXED BY word_idx
        #          WHERE word=?""", (w, ))
        r = c.fetchall()
        if len(r) > 0:
            #PAD (word_index=0, rowid=1) UNK (word_index=1, rowid=2)
            word_ids.append(r[0][0] - 1) #rowid is 1+ word_index...
        else:
            word_ids.append(1)
    # Pad with zeros if necessary
    num_words = len(word_ids)
    if num_words < sentence_length:
        word_ids += [0]*(sentence_length-num_words)
    vectordb.close()
    return word_ids #, num_words

def yieldRouge(CorpusFile, snippets_only=True):
    """yield ROUGE scores of all sentences in corpus
    >>> rouge = yieldRouge('BioASQ-trainingDataset5b.json')
    >>> target = (0, '15829955', 0, {'N-1': 0.1519, 'S4': 0.0, 'SU4': 0.04525, 'N-2': 0.0, 'L': 0.0}, 'The identification of common variants that contribute to the genesis of human inherited disorders remains a significant challenge.')
    >>> next(rouge) == target
    True
    >>> target2 = (0, '15829955', 1, {'N-1': 0.31915, 'S4': 0.02273, 'SU4': 0.09399, 'N-2': 0.13043, 'L': 0.04445}, 'Hirschsprung disease (HSCR) is a multifactorial, non-mendelian disorder in which rare high-penetrance coding sequence mutations in the receptor tyrosine kinase RET contribute to risk in combination with mutations at other genes.')
    >>> next(rouge) == target2
    True
    """
    data = json.load(open(CorpusFile, encoding='utf-8'))['questions']
    for qi in range(len(data)):
        if 'snippets' not in data[qi].keys():
            print("Warning: No snippets in question %s" % data[qi]['body'])
            continue
        ai = 0
        try:
            if type(data[qi]['ideal_answer']) == list:
                ideal_answers = data[qi]['ideal_answer']
            else:
                ideal_answers = [data[qi]['ideal_answer']]
        except:
            ideal_answers = [""]
        for (pubmedid, senti, sent) in yield_candidate_text(data[qi], snippets_only=snippets_only):                
            rouge_scores = [rouge_engine.get_scores(h, sent)[0] for h in ideal_answers]
            rouge_value = max([(r['rouge-2']['f'] + r['rouge-l']['f'])/2 for r in rouge_scores])
            yield (qi, pubmedid, senti, rouge_value, sent)

def saveRouge(corpusfile, outfile, snippets_only=True):
    "Compute and save the ROUGE scores of the individual snippet sentences"
    try:
        with open(outfile,'w') as f:
            writer = csv.writer(f)
    #        writer.writerow(('qid','snipid','sentid','N1','N2','L','S4','SU4','sentence text'))
            writer.writerow(('qid','pubmedid','sentid','L','sentence text'))
            for (qi,qsnipi,senti,F,sent) in yieldRouge(corpusfile, snippets_only=snippets_only):
                writer.writerow((qi,qsnipi,senti,F,sent))
    except:
        pass


class NNModel():
    def __init__(self, vocabulary_size):
        X_state = keras.layers.Input(shape=(4*vocabulary_size + 1,))
        Q_state = keras.layers.Input(shape=(vocabulary_size,))
        reward = keras.layers.Input(shape=(1,))
        all_inputs = keras.layers.Concatenate()([X_state, Q_state])
        self.episode = tf.placeholder(tf.float32)
        hidden = keras.layers.Dense(N_HIDDEN, activation="relu")(all_inputs)
        outputs = keras.layers.Dense(1, activation="sigmoid")(hidden)

        def custom_loss(y_true, y_predict):
            "From https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
            #log_lik = K.log(y_true * (y_true - y_predict) + (1 - y_true) * (y_true + y_predict))
            loss = - (y_true * K.log(y_predict) + (1 - y_true) * K.log(1 - y_predict)) # Using the standard cross-entropy formula
            return K.mean(loss * reward, keepdims=True)

        self.model_train = keras.models.Model([X_state, Q_state, reward], outputs)
        self.model_train.compile(loss=custom_loss, optimizer=keras.optimizers.Adam())

        self.model_predict = keras.models.Model([X_state, Q_state], outputs)

class NNModelEmbeddings():

    def __init__(self, vocabulary_size):
        keep_prob = 0.5 ##CHANGE????
        
        ###X1 Sentence (All Text)
        X1_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X')
        embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(X1_input)
        X1_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s1)
        #X1_dropout = keras.layers.Dropout(1 - keep_prob)(X1_meanembeddings)
        
        ###X2 Sentence (This Sentence)
        X2_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X2')
        embedding_s2 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(X2_input)
        X2_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s2)
        #X2_dropout = keras.layers.Dropout(1 - keep_prob)(X2_meanembeddings)
        
        ###X3 Sentence (Next Sentences)
        X3_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X3')
        embedding_s3 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(X3_input)
        X3_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s3)
        #X3_dropout = keras.layers.Dropout(1 - keep_prob)(X3_meanembeddings)
        
        ###X4 Sentence (Summary Sentence - ?)
        X4_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X4')
        embedding_s4 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(X4_input)
        X4_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s4)
        #X4_dropout = keras.layers.Dropout(1 - keep_prob)(X4_meanembeddings)
        
        ###X5 Sentence length / position in document
        X5_state = keras.layers.Input(shape=(1,))


        ###Q Question
        Q_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='Q')
        embedding_q = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(Q_input)
        embedding_q_reduction = keras.layers.GlobalAveragePooling1D()(embedding_q)
        Q_output = keras.layers.Dropout(1 - keep_prob)(embedding_q_reduction)
        
        
        # Similarity
        #sim = keras.layers.Multiply()([X_dropout, Q_output])
        
        
        #X_state = keras.layers.Input(shape=(4*vocabulary_size + 1,))
        #Q_state = keras.layers.Input(shape=(vocabulary_size,))
        reward = keras.layers.Input(shape=(1,))
        all_inputs = keras.layers.Concatenate()([X1_meanembeddings, X2_meanembeddings, X3_meanembeddings, X4_meanembeddings, X5_state, Q_output])
        #all_inputs = keras.layers.Concatenate()([X1_dropout, X2_dropout, X3_dropout, X4_dropout, X5_state, Q_output])
        self.episode = tf.placeholder(tf.float32)
        hidden = keras.layers.Dense(N_HIDDEN, activation="relu")(all_inputs)
        outputs = keras.layers.Dense(1, activation="sigmoid")(hidden)

        def custom_loss(y_true, y_predict):
            "From https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
            #log_lik = K.log(y_true * (y_true - y_predict) + (1 - y_true) * (y_true + y_predict))
            loss = - (y_true * K.log(y_predict) + (1 - y_true) * K.log(1 - y_predict)) # Using the standard cross-entropy formula
            return K.mean(loss * reward, keepdims=True)

        self.model_train = keras.models.Model([X1_input, X2_input, X3_input, X4_input, X5_state, Q_input, reward], outputs)
        self.model_train.compile(loss=custom_loss, optimizer=keras.optimizers.Adam())

        self.model_predict = keras.models.Model([X1_input, X2_input, X3_input, X4_input, X5_state, Q_input], outputs)

def baseline(testfile=EVALFILE):
    """Evaluate a baseline that returns the first n sentences"""
    nanswers = {"summary": 6,
                "factoid": 2,
                "yesno": 2,
                "list": 3}
    env = Environment(jsonfile='training8b.json')
    if type(testfile) == None:
        alldata = list(range(len(env.data)))
        np.random.shuffle(alldata)
        split_boundary = int(len(alldata)*.8)
        train_indices = alldata[:split_boundary]
        test_indices = alldata[split_boundary:]
    else:
        with open(testfile) as f:
            reader = csv.DictReader(f)
            test_indices = list(set(int(l['QID'].split('-')[0]) for l in reader))

    scores = []
    for x in test_indices:
        observation = env.reset(x)
        n = nanswers[env.qtype]
        if len(env.candidates) == 0:
            continue

        while not observation['done']:
            this_candidate = observation['next_candidate']
            if this_candidate < n:
                action = 1
            else:
                action = 0
            observation = env.step(action)
        reward = observation['reward']
        scores.append(reward)
    return np.mean(scores)

def train():
    try:
        with open(LOGFILE, 'w') as f:
            f.write("episode,reward,QID,summary\n")

        with open(EVALFILE, 'w') as f:
            f.write("episode,reward,QID,summary\n")
    except:
        pass
    env = Environment(jsonfile='training8b.json')
    alldata = list(range(len(env.data)))
    np.random.shuffle(alldata)
    split_boundary = int(len(alldata)*.8)
    train_indices = alldata[:split_boundary]
    test_indices = alldata[split_boundary:]

    # train tf.idf

    if MEAN_EMBEDDINGS:
        nnModel = NNModelEmbeddings(SENTENCE_LENGTH)
    else:
        if RESTORE:
            with open(TFIDF_FILENAME,'rb') as f:
                    tfidf = pickle.load(f)
        else:
            if VERBOSE > 0:
                print("Training tf.idf")
            tfidf_train_text = [env.data[x]['body'] for x in train_indices]
            tfidf_train_text += [c[2] for x in train_indices for c in yield_candidate_text(env.data[x], snippets_only=True)]
            ideal_summaries_sentences = []
            for x in train_indices:
                try:
                    ideal_summaries = env.data[x]['ideal_answer']
                except:
                    ideal_summaries = ""
                if type(ideal_summaries) != list:
                    ideal_summaries = [ideal_summaries]
                for ideal_sum in ideal_summaries:
                    ideal_summaries_sentences += sent_tokenize(ideal_sum)
            tfidf_train_text += ideal_summaries_sentences
            #print(len(tfidf_train_text))
            #print(tfidf_train_text[:10])
            tfidf = TfidfVectorizer(tokenizer=my_tokenize)
            tfidf.fit(tfidf_train_text)
            with open(TFIDF_FILENAME,'wb') as f:
                pickle.dump(tfidf,f)
        nnModel = NNModel(len(tfidf.get_feature_names()))

    if VERBOSE > 0:
        print("Training REINFORCE")

    if RESTORE:
        #nnModel.model_train = load_model(BEST_CHECKPOINT_PATH)
        nnModel.model_train.load_weights(BEST_CHECKPOINT_PATH)
        nnModel.model_predict.load_weights(BEST_CHECKPOINT_PATH)

    while True:
        train_x = np.random.choice(train_indices)
        observation = env.reset(train_x) # Reset to a random question

        if len(env.candidates) > 0:
            break
    if MEAN_EMBEDDINGS:
        embeddings_all_candidates = [one_sentence_to_ids(i) for i in env.candidates]
        embeddings_all_text = [one_sentence_to_ids(" ".join(env.candidates))]
    else:
        tfidf_all_candidates = tfidf.transform(env.candidates)
        tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]
    
    
    best_test_score = 0.0
    #nnModel.model_train.save(BEST_CHECKPOINT_PATH)

    states = []
    states2 = []
    states3 = []
    states4 = []
    states5 = []
    questions = []
    actions = []

    episode = 0
    while True:
        # The following code is based on "Policy Gradients"
        # at https://github.com/ageron/handson-ml/blob/master/16_reinforcement_learning.ipynb with adaptation to
        # Keras from  https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
        this_candidate = observation['next_candidate']
        if MEAN_EMBEDDINGS:
            embeddings_this_candidate = [embeddings_all_candidates[this_candidate]]
            embeddings_remaining_candidates = [one_sentence_to_ids(" ".join(env.candidates[this_candidate + 1:]))]
            embeddings_summary = [one_sentence_to_ids(" ".join([env.candidates[x] for x in observation['summary']]))]
            embeddings_question = [one_sentence_to_ids(env.question)]
            #print(embeddings_question.shape)
            #XState = np.hstack([])
            predict = nnModel.model_predict.predict([
                                csr_matrix(embeddings_all_text).todense(),              ###X1 Sentence (All Text)
                                csr_matrix(embeddings_this_candidate).todense(),        ###X2 Sentence (This Sentence)
                                csr_matrix(embeddings_remaining_candidates).todense(),  ###X3 Sentence (Next Sentences)
                                csr_matrix(embeddings_summary).todense(),               ###X4 Sentence (Summary Sentence - ?)
                                csr_matrix([[len(observation['summary'])]]).todense(),  ###X5 Sentence length / position in document
                                csr_matrix(embeddings_question).todense()])             ###Q Question     (Don't apply 'sim' Similarity / learn)
        else:
            tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
            tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0, :]
            tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0, :]
            tfidf_question = tfidf.transform([env.question]).todense()[0,:]
            #print(tfidf_question.shape)
            XState = np.hstack([tfidf_all_text,
                                tfidf_this_candidate,
                                tfidf_remaining_candidates,
                                tfidf_summary,
                                [[len(observation['summary'])]]])
            predict = nnModel.model_predict.predict([XState, tfidf_question])
        # print("Prediction:", predict)
        predict = predict[0][0]
        perturb = NOISE*3000/(3000+episode) # decrease the perturbation with each training episode
        p_left_and_right = [(predict+perturb)/(1+2*perturb), (1-predict+perturb)/(1+2*perturb)]
        action_val = np.random.choice(2, p=p_left_and_right)

        if MEAN_EMBEDDINGS:
            states.append(csr_matrix(embeddings_all_text).todense())               ###X1 Sentence (All Text)
            states2.append(csr_matrix(embeddings_this_candidate).todense())        ###X2 Sentence (This Sentence)
            states3.append(csr_matrix(embeddings_remaining_candidates).todense())  ###X3 Sentence (Next Sentences)
            states4.append(csr_matrix(embeddings_summary).todense())               ###X4 Sentence (Summary Sentence - ?)
            states5.append(csr_matrix([[len(observation['summary'])]]).todense())  ###X5 Sentence length / position in document
            questions.append(csr_matrix(embeddings_question).todense())            ###Q Question     (Don't apply 'sim' Similarity / learn)
        else:
            states.append(XState)
            questions.append(tfidf_question)
        actions.append(1 - action_val)

        observation = env.step(action_val)

        if observation['done']:
            # reward all actions that lead to the summary
            reward = observation['reward']
            print("Episode: %i, reward: %f" % (episode, reward))
            try:
                with open(LOGFILE, 'a') as f:
                    f.write('%i,%f,%i-%s,"%s"\n' % (episode, reward, env.qid, env.qtype,
                                                " ".join([str(x) for x in observation['summary']])))
            except:
                pass
            if MEAN_EMBEDDINGS:
                loss = nnModel.model_train.train_on_batch([np.vstack(states), np.vstack(states2), np.vstack(states3), np.vstack(states4), np.vstack(states5), np.vstack(questions), np.array([reward]*len(states))],
                                                      actions)
            else:
                loss = nnModel.model_train.train_on_batch([np.vstack(states), np.vstack(questions), np.array([reward]*len(states))],
                                                      actions)

            episode += 1
            if episode % SAVE_EPISODES == 0:
                print("Saving checkpoint in %s" % (CHECKPOINT_PATH))
                #nnModel.model_train.save(CHECKPOINT_PATH)
                print("Testing results")
                test_results = []
                for test_x in test_indices:
                    observation = env.reset(test_x)

                    if len(env.candidates) == 0:
                        continue

                                    
                    if MEAN_EMBEDDINGS:
                        embeddings_all_candidates = [one_sentence_to_ids(i) for i in env.candidates]
                        embeddings_all_text = [one_sentence_to_ids(" ".join(env.candidates))]
                    else:
                        tfidf_all_candidates = tfidf.transform(env.candidates)
                        tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]

                    while not observation['done']:
                        this_candidate = observation['next_candidate']
                        if MEAN_EMBEDDINGS:
                            embeddings_this_candidate = [embeddings_all_candidates[this_candidate]]
                            embeddings_remaining_candidates = [one_sentence_to_ids(" ".join(env.candidates[this_candidate + 1:]))]
                            embeddings_summary = [one_sentence_to_ids(" ".join([env.candidates[x] for x in observation['summary']]))]
                            embeddings_question = [one_sentence_to_ids(env.question)]
                            #print(embeddings_question.shape)
                            #XState = np.hstack([])
                            output_val = nnModel.model_predict.predict([
                                                csr_matrix(embeddings_all_text).todense(),              ###X1 Sentence (All Text)
                                                csr_matrix(embeddings_this_candidate).todense(),        ###X2 Sentence (This Sentence)
                                                csr_matrix(embeddings_remaining_candidates).todense(),  ###X3 Sentence (Next Sentences)
                                                csr_matrix(embeddings_summary).todense(),               ###X4 Sentence (Summary Sentence - ?)
                                                csr_matrix([[len(observation['summary'])]]).todense(),  ###X5 Sentence length / position in document
                                                csr_matrix(embeddings_question).todense()])             ###Q Question     (Don't apply 'sim' Similarity / learn)
                        else:
                            tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
                            tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0, :]
                            tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0, :]
                            tfidf_question = tfidf.transform([env.question]).todense()[0,:]
                            #print(tfidf_question.shape)
                            XState = np.hstack([tfidf_all_text,
                                                tfidf_this_candidate,
                                                tfidf_remaining_candidates,
                                                tfidf_summary,
                                                [[len(observation['summary'])]]])
                            output_val = nnModel.model_predict.predict([XState, tfidf_question])
                        
                        action_val = 0
                        if output_val < 0.5:
                            action_val = 1
                        observation = env.step(action_val)
                    reward = observation['reward']
                    test_results.append(reward)
                    try:
                        with open(EVALFILE, 'a') as f:
                            f.write('%i,%f,%i-%s,"%s"\n' % (episode,reward,env.qid,env.qtype," ".join([str(x) for x in observation['summary']])))
                    except:
                        pass
                mean_test_results = np.mean(test_results)
                print("Mean of evaluation results:", mean_test_results)
                if mean_test_results > best_test_score:
                    print("Test results improved; saving model")
                    #nnModel.model_train.save(BEST_CHECKPOINT_PATH)
                    best_test_score = mean_test_results

            # Pick next training question
            while True:
                train_x = np.random.choice(train_indices)
                observation = env.reset(train_x) # Reset to a random question

                if len(env.candidates) > 0:
                    break

            states = []
            states2 = []
            states3 = []
            states4 = []
            states5 = []
            questions = []
            actions = []
            if MEAN_EMBEDDINGS:
                embeddings_all_candidates = [one_sentence_to_ids(i) for i in env.candidates]
                embeddings_all_text = [one_sentence_to_ids(" ".join(env.candidates))]
            else:
                tfidf_all_candidates = tfidf.transform(env.candidates)
                tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]

def test(outfile=None, jsonfile=None):
    env = Environment(jsonfile='training8b.json')
    try:
        with open(EVALFILE) as f:
            reader = csv.DictReader(f)
            test_indices = list(set(int(l['QID'].split('-')[0]) for l in reader))
        with open(TFIDF_FILENAME, 'rb') as f:
            tfidf = pickle.load(f)
    except:
        pass
    if MEAN_EMBEDDINGS:
        nnModel = NNModelEmbeddings(SENTENCE_LENGTH)
    else:
        nnModel = NNModel(len(tfidf.get_feature_names()))
    nnModel.model_predict.load_weights(CHECKPOINT_PATH)
    result_json = []
    test_results = []
    #test_indices = test_indices[:20]

    for test_x in test_indices:
        observation = env.reset(test_x)
        if len(env.candidates) == 0:
            continue


        if MEAN_EMBEDDINGS:
            embeddings_all_candidates = [one_sentence_to_ids(i) for i in env.candidates]
            embeddings_all_text = [one_sentence_to_ids(" ".join(env.candidates))]
            predicted_probs = []
            while not observation['done']:
                this_candidate = observation['next_candidate']
                embeddings_this_candidate = [embeddings_all_candidates[this_candidate]]
                embeddings_remaining_candidates = [one_sentence_to_ids(" ".join(env.candidates[this_candidate + 1:]))]
                embeddings_summary = [one_sentence_to_ids(" ".join([env.candidates[x] for x in observation['summary']]))]
                embeddings_question = [one_sentence_to_ids(env.question)]
                #XState = np.hstack([])
                output_val = nnModel.model_predict.predict([
                                    csr_matrix(embeddings_all_text).todense(),              ###X1 Sentence (All Text)
                                    csr_matrix(embeddings_this_candidate).todense(),        ###X2 Sentence (This Sentence)
                                    csr_matrix(embeddings_remaining_candidates).todense(),  ###X3 Sentence (Next Sentences)
                                    csr_matrix(embeddings_summary).todense(),               ###X4 Sentence (Summary Sentence - ?)
                                    csr_matrix([[len(observation['summary'])]]).todense(),  ###X5 Sentence length / position in document
                                    csr_matrix(embeddings_question).todense()])             ###Q Question     (Don't apply 'sim' Similarity / learn)
                predicted_probs.append(output_val)
                action_val = 0
                if output_val < 0.5:
                    action_val = 1
                observation = env.step(action_val)
        else:
            tfidf_all_candidates = tfidf.transform(env.candidates)
            tfidf_all_text = tfidf.transform([" ".join(env.candidates)]).todense()[0,:]
            predicted_probs = []
            while not observation['done']:
                this_candidate = observation['next_candidate']
                tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
                tfidf_remaining_candidates = tfidf.transform([" ".join(env.candidates[this_candidate + 1:])]).todense()[0,:]
                tfidf_summary = tfidf.transform([" ".join([env.candidates[x] for x in observation['summary']])]).todense()[0,:]
                tfidf_question = tfidf.transform([env.question]).todense()[0,:]
                #print(tfidf_question.shape)
                XState = np.hstack([tfidf_all_text,
                                    tfidf_this_candidate,
                                    tfidf_remaining_candidates,
                                    tfidf_summary,
                                    [[len(observation['summary'])]]])
                output_val = nnModel.model_predict.predict([XState, tfidf_question])
                predicted_probs.append(output_val)
                action_val = 0
                if output_val < 0.5:
                    action_val = 1
                observation = env.step(action_val)
        reward = observation['reward']
        test_results.append(reward)
        print("Reward:", reward)
        if outfile:
            with open(outfile,'a') as f:
                f.write(','.join(str(p[0][0]) for p in predicted_probs))
                f.write('\n')
        # Create information for JSON output
        if jsonfile:
            if env.qtype == "yesno":
                exactanswer = "yes"
            else:
                exactanswer = ""
            result_json.append({"id": env.id,
                                "ideal_answer": " ".join([env.candidates[x] for x in observation['summary']]),
                                "exact_answer": exactanswer})

    if jsonfile:
        print("Saving results in file %s" % jsonfile)
        try:
            with open(jsonfile, 'w') as f:
                f.write(json.dumps({"questions": result_json}, indent=2))
        except:
            pass

    print("Mean of evaluation results:", np.mean(test_results))

def bioasq_run(test_data='phaseB_5b_01.json', #BioASQ-training7b.json
               output_filename='bioasq-out-rl.json'):
    """Run model for BioASQ"""
    print("Running BioASQ")
    if not MEAN_EMBEDDINGS:
        with open(BEST_TFIDF_FILENAME, 'rb') as f:
            tfidf = pickle.load(f)
    testset = json.load(open(test_data, encoding='utf-8'))['questions']
    if DEBUG:
        testset = testset[:10]
    result = []
    if MEAN_EMBEDDINGS:
        nnModel = NNModelEmbeddings(SENTENCE_LENGTH)
    else:
        nnModel = NNModel(len(tfidf.get_feature_names()))
    nnModel.model_predict.load_weights(BEST_CHECKPOINT_PATH)
    for r in testset:
        test_question = r['body']
        test_id = r['id']
        test_candidates = [sent
                           for pubmedid, senti, sent
                           in yield_candidate_text(r)]
        test_candidates = test_candidates[:20]
        if len(test_candidates) == 0:
            print("Warning: no text to summarise")
            test_summary = ''
        else:
            if MEAN_EMBEDDINGS:
                embeddings_all_candidates = [one_sentence_to_ids(i) for i in test_candidates]
                embeddings_all_text = [one_sentence_to_ids(" ".join(test_candidates))]
                test_summary = ''
                len_summary = 0
                output_probs = []
                for this_candidate in range(len(test_candidates)):
                    embeddings_this_candidate = [embeddings_all_candidates[this_candidate]]
                    embeddings_remaining_candidates = [one_sentence_to_ids(" ".join(test_candidates[this_candidate + 1:]))]
                    embeddings_summary = [one_sentence_to_ids(test_summary)]
                    embeddings_question = [one_sentence_to_ids(test_question)]
                    #XState = np.hstack([])
                    output_val = nnModel.model_predict.predict([
                                        csr_matrix(embeddings_all_text).todense(),              ###X1 Sentence (All Text)
                                        csr_matrix(embeddings_this_candidate).todense(),        ###X2 Sentence (This Sentence)
                                        csr_matrix(embeddings_remaining_candidates).todense(),  ###X3 Sentence (Next Sentences)
                                        csr_matrix(embeddings_summary).todense(),               ###X4 Sentence (Summary Sentence - ?)
                                        csr_matrix([[len_summary]]).todense(),             ###X5 Sentence length / position in document
                                        csr_matrix(embeddings_question).todense()])             ###Q Question     (Don't apply 'sim' Similarity / learn)
                    output_probs.append(1-output_val)
                    if output_val < 0.5:
                        len_summary += 1
                        if test_summary == '':
                            test_summary = test_candidates[this_candidate]
                        else:
                            test_summary += " " + test_candidates[this_candidate]
            else:
                tfidf_all_candidates = tfidf.transform(test_candidates)
                tfidf_all_text = tfidf.transform([" ".join(test_candidates)]).todense()[0,:]
                test_summary = ''
                len_summary = 0
                output_probs = []
                for this_candidate in range(len(test_candidates)):
                    tfidf_this_candidate = tfidf_all_candidates[this_candidate].todense()
                    tfidf_remaining_candidates = tfidf.transform([" ".join(test_candidates[this_candidate + 1:])]).todense()[0,:]
                    tfidf_summary = tfidf.transform([test_summary]).todense()[0,:]
                    tfidf_question = tfidf.transform([test_question]).todense()[0,:]
                    XState = np.hstack([tfidf_all_text,
                                        tfidf_this_candidate,
                                        tfidf_remaining_candidates,
                                        tfidf_summary,
    #                                  [[len(observation['summary']), this_candidate]]])
                                        [[len_summary]]])
                    output_val = nnModel.model_predict.predict([XState, tfidf_question])
                    output_probs.append(1-output_val)
                    if output_val < 0.5:
                        len_summary += 1
                        if test_summary == '':
                            test_summary = test_candidates[this_candidate]
                        else:
                            test_summary += " " + test_candidates[this_candidate]
        if test_summary == '' and len(test_candidates) > 0:
            print("Warning: no summary produced; returning top sentence %i" % np.argmax(output_probs))
            #print("Output probabilities are:")
            #print(output_probs)
            test_summary = test_candidates[np.argmax(output_probs)]
        if r['type'] == "yesno":
            exactanswer = "yes"
        else:
            exactanswer = ""

        result.append({"id": test_id,
                       "ideal_answer": test_summary,
                       "exact_answer": exactanswer})
    print("Saving results in file %s" % output_filename)
    try:
        with open(output_filename, 'w') as f:
            f.write(json.dumps({"questions": result}, indent=2))
    except:
        pass


if __name__ == "__main__":
    train()
    #bioasq_run()
