"""Interface to obtain ROUGE scores"""
import os
from subprocess import Popen, PIPE
import codecs

def get_scores(models, summary, xml_rouge_filename="rouge.xml"):
    """Return the ROUGE scores of the peer given the models
    - models is a list of summaries
    - summary is a computer-generated summary
    >>> models = ["This is sample summary 1", "And this this another sample summary"]
    >>> summary = "My sample summary"
    >>> get_scores(models, summary)
    {'N-1': 0.47059, 'N-2': 0.30769, 'L': 0.0, 'S4': 0.0, 'SU4': 0.18182}
    """
    assert type(models) == list
    assert type(summary) == str
    
    with open(xml_rouge_filename,'w') as rougef:
        rougef.write("""<ROUGE-EVAL version="1.0">
 <EVAL ID="1">
 <MODEL-ROOT>
 ../rouge/models
 </MODEL-ROOT>
 <PEER-ROOT>
 ../rouge/summaries
 </PEER-ROOT>
 <INPUT-FORMAT TYPE="SEE" />
 <PEERS>
""")
        rougef.write('  <P ID="A">summary_%s_1</P>\n' % xml_rouge_filename.replace(".xml",""))
        peerfilename = os.path.join('..', 'rouge', 'summaries', 'summary_%s_1' % xml_rouge_filename.replace(".xml",""))
        with codecs.open(peerfilename,'w','utf-8') as fout:
            text = '<html>\n<head>\n<title>system</title>\n</head>\n<body bgcolor="white">\n<a name="1">[1]</a> <a href="#1" id=1>{0}</a></body>\n</html>'.format(summary)
            fout.write(text + "\n")
            
        rougef.write("""</PEERS>
 <MODELS>
""")
        for mi, modeltext in enumerate(models):
            rougef.write('  <M ID="%i">model_%s_%i</M>\n' % (mi, xml_rouge_filename.replace(".xml","") , mi))
            modelfilename = os.path.join('..', 'rouge', 'models', 'model_%s_%s' % (xml_rouge_filename.replace(".xml",""), str(mi)))
            with codecs.open(modelfilename,'w','utf-8') as fout:
                text = '<html>\n<head>\n<title>system</title>\n</head>\n<body bgcolor="white">\n<a name="1">[1]</a> <a href="#1" id=1>{0}</a></body>\n</html>'.format(modeltext)
                fout.write(text + "\n")
        rougef.write("""</MODELS>
</EVAL>
</ROUGE-EVAL>""")
        
    ROUGE_CMD = 'perl ../rouge/RELEASE-1.5.5/ROUGE-1.5.5.pl -e ../rouge/RELEASE-1.5.5/data -c 95 -2 4 -u -x -n 4 -a ' + xml_rouge_filename
    stream = Popen(ROUGE_CMD, shell=True, stdout=PIPE).stdout
    lines = stream.readlines()
    #for l in lines:
    #    print(l)
    stream.close()
    F = {'N-1':float(lines[3].split()[3]),
         'N-2':float(lines[7].split()[3]),
         'L':float(lines[11].split()[3]),
         'S4':float(lines[15].split()[3]),
         'SU4':float(lines[19].split()[3])}
    #print("Resulting ROUGE:", F)
    return F
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
    
