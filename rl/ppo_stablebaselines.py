#https://towardsdatascience.com/creating-a-custom-openai-gym-environment-for-stock-trading-be532be3910e

import gym
import json
import datetime as dt
from stable_baselines.common.policies import MlpPolicy, MlpLstmPolicy, MlpLnLstmPolicy, CnnPolicy, CnnLstmPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.ppo1.pposgd_simple import PPO1
from stable_baselines.ppo2.ppo2 import PPO2
from stable_baselines.deepq.dqn import DQN
from stable_baselines.a2c.a2c import A2C
from stable_baselines.acer.acer_simple import ACER
from stable_baselines.trpo_mpi.trpo_mpi import TRPO
from stable_baselines.deepq.policies import DQNPolicy
from stable_baselines.deepq.policies import MlpPolicy as DMlpPolicy
from stable_baselines.deepq.policies import CnnPolicy as DCnnPolicy
from stable_baselines import HER, SAC, DDPG
from stable_baselines.ddpg import AdaptiveParamNoiseSpec
import random
from gym import spaces
import pandas as pd
import numpy as np
import os
from nltk import sent_tokenize
import traceback
import argparse
from xml_abstract_retriever import getAbstract
import tensorflow as tf
from tensorflow import keras
import sys
import sqlite3
from scipy.sparse import csr_matrix
from multiprocessing import Pool
from multiprocessing import Process
from functools import partial
import rouge
from rl.rouge_engine import get_scores as perl_scores
from stable_baselines.common.policies import ActorCriticPolicy
from stable_baselines.common import explained_variance, ActorCriticRLModel, tf_util, TensorboardWriter
import codecs
import csv
from sklearn.model_selection import KFold
from stable_baselines.bench import Monitor
from stable_baselines.results_plotter import load_results, ts2xy
import datetime

#from .acer2 import ACER as ACER2
from .ppo3 import PPO2 as PPO3
#from .ppo4 import PPO2 as PPO4
#from .ppo5 import PPO2 as PPO5

DEBUG = False
VERBOSE = 1

#0.990 - Dota learner uses 0.997-0.998...?

parser = argparse.ArgumentParser(description='Reinforcement Learning with Stable_Baselines for BioASQ')
parser.add_argument('--observe', type=str, default='EMBEDDINGS', help='The features to be observed (INDEX, TFIDF, EMBEDDINGS)')
parser.add_argument('--rouge', type=str, default='PERL', help='The rouge version (PERL, PYTHON)')
parser.add_argument('--load', type=str, default='NONE', help='Import the existing model to either continue training, or test it (NONE, RETRAIN, TEST, BioASQ)')
parser.add_argument('--model', type=str, default='PPO2', help='The model to test with (PPO1, PPO2, TRPO, A2C, DQN, DDPG)')
parser.add_argument('--policy', type=str, default='MLP200', help='The Policy to use and size of the hidden layers (CUST200, MLP64, MLP200, MLP400, MLPLSTM200, MLPLSTMLN200)')
parser.add_argument('--append', type=str, default='', help='Append the saved file name with a string')
parser.add_argument('--act', type=str, default='RELU', help='Activation function')
parser.add_argument('--timesteps', type=int, default=500000, help='The number of timesteps to perform with the model (default 500000)')
parser.add_argument('--cpus', type=int, default=4, help='The number of paralell cpus or environments (default 4)')
parser.add_argument('--gamma', type=float, default=0.99, help='The gamma discount factor for balancing fastest completion and highest rewards (default 0.99)')
parser.add_argument('--seed', type=int, default=123, help='The seed for splitting train/test sets')
parser.add_argument('--fold', type=int, default=0, help='The fold for splitting train/test sets')
parser.add_argument('--epochs', type=int, default=20, help='The number of epochs for CLASSIFICATION model training')
parser.add_argument('--weight', type=float, default=1, help='The weight of increase/decrease for "correct" classification weights for CLASSIFICATION (default 1)')
parser.add_argument('--batch_size', type=int, default=1000, help='The "horizon", or number of steps per batch for PPO')
parser.add_argument('--minibatches', type=int, default=4, help='The number of minibatches to divide batches into (4 by default)')
parser.add_argument('--topn', type=int, default=5, help='Number of top n labels to train for CLASSIFICATION')
parser.add_argument('--thread_test', dest='thread_test', action='store_true')
parser.set_defaults(thread_test=False)
parser.add_argument('--continuous_action', dest='continuous_action', action='store_true')
parser.add_argument('--discrete_action', dest='continuous_action', action='store_false')
parser.set_defaults(continuous_action=False)
parser.add_argument('--early_reward', dest='early_reward', action='store_true')
parser.set_defaults(early_reward=False)
parser.add_argument('--reverse', dest='reverse', action='store_true')
parser.set_defaults(reverse=False)
parser.add_argument('--linear', dest='linear', action='store_true')
parser.set_defaults(linear=False)
parser.add_argument('--baseline', dest='baseline', action='store_true')
parser.set_defaults(baseline=False)
parser.add_argument('--8b', dest='bioasq8b', action='store_true')
parser.add_argument('--bioasq8b', dest='bioasq8b', action='store_true')
parser.set_defaults(bioasq8b=False)
parser.add_argument('--bestaction', dest='bestaction', action='store_true')
parser.set_defaults(bestaction=False)
parser.set_defaults(bioasq8b=False)
args = parser.parse_args()

#PPO (BEST): python -m rl.ppo_stablebaselines --8b --observe EMBEDDINGS --model PPO2 --batch_size=1000 --append _best --bestaction
#PPO-LSTM: python -m rl.ppo_stablebaselines --continuous_action --observe ALL --model PPO2 --policy LSTM --batch_size=1000
#BERT: python -m rl.ppo_stablebaselines --8b --observe BERT --model PPO2 --batch_size=1000 --bestaction
if args.model == "PPO":
    args.model = "PPO2"
if args.model == "PPOMOD":
    args.model = "PPO3"
assert args.observe in ['INDEX', 'SUMMARY', 'SUMNEW', 'TFIDF', 'EMBEDDINGS', 'ALL', 'SUMMARUNNER', 'NEW', 'EMBEDDINGS200', 'OLD', 'FLAGRUN', 'BERT'] #'SUMMARUNNERTFIDF', 'TFIDFORIG', 'EMBEDDINGSORIG'
assert args.rouge in ['PERL', 'PYTHON']
assert args.model in ['PPO1', 'PPO2', 'PPO3', 'TRPO', 'A2C', 'DQN', 'DDPG', 'HER', 'SAC', 'ACER', 'FIRSTN', 'LASTN', 'RANDOM', 'ALL', 'REINFORCE', 'CLASSIFICATION']
assert args.load in ['NONE', 'RETRAIN', 'TEST', 'BioASQ']
assert args.act in ['RELU', 'SIGMOID', 'TANH', 'NONE']
assert args.policy in ['MLP64', 'MLP200', 'MLP1', 'MLP4CPU', 'MLP400', 'MLPLSTM200', 'MLPLSTMLN200', 'CUST200', 'REINFORCE200', 'CUSTNN', 'CUSTNNCPU', 'CUSTNNLOAD', 'LSTM', 'BERT'] #, 'CNN200', 'CNNLSTM200' - CNN is only for images!
SAVE_PATH = "RL_model_save_%s_%s%s%s%s%s"  % (args.model, args.observe, args.policy, args.act, args.batch_size, args.append)
GAMMA = args.gamma #Discount Factor - importance of finding SHORTEST solution over best reward
STEPS = 50
SENTENCE_LENGTH = 300
EMBEDDINGS = 100 #100/200 not that different - mainly memory needed!
if args.observe == 'EMBEDDINGS200':
    EMBEDDINGS = 200
    args.observe = 'EMBEDDINGS'

if __name__ == "__main__": 
    if args.seed == 0:
        args.seed = random.randint(1,999999)
    with open("%s_test.txt" % SAVE_PATH, "a") as f:
        f.write("#SEED: %s (fold %s)\n" % (args.seed, args.fold))
        f.write("#TIME: %s\n" % str(datetime.datetime.now()))
print(args)



def embeddings_one_sentence(row):
    return [float(x) for x in row[1].split()]
def setup_embeddings(_pool=True):
    global VECTORS, DB, VOCABULARY, meanEmbeddings, EMBEDDINGS
    VECTORS = 'allMeSH_2016_%i.vectors.txt' % EMBEDDINGS
    DB = 'word2vec_%i.db' % EMBEDDINGS
    with open(VECTORS) as v:
        VOCABULARY = int(v.readline().strip().split()[0]) + 2
    
    vectordb = sqlite3.connect(DB)
    print("Database %s opened" % DB)
    c = vectordb.cursor()
    c_iterator = c.execute("""SELECT word, data FROM vectors""")
    print("Loading word embeddings")
    if _pool:
        with Pool() as pool:
            embeddings_list = pool.map(embeddings_one_sentence,
                                       c_iterator)
    else:
        embeddings_list = [embeddings_one_sentence(i) for i in c_iterator]
    print("Word embeddings loaded")
    vectordb.close()

    class MeanOfEmbeddings():
        def __init__(self):
            ###X1 Sentence (All Text)
            X1_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X')
            embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                     weights=[np.array(embeddings_list)],
                                                     trainable=False)(X1_input)
            X1_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s1)

            self.model = keras.models.Model([X1_input], X1_meanembeddings)
    meanEmbeddings = MeanOfEmbeddings()
    if args.observe == 'ALL':
        class AllEmbeddings():
            def __init__(self):
                ###X1 Sentence (All Text)
                X1_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X')
                embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                         weights=[np.array(embeddings_list)],
                                                         trainable=False)(X1_input)
                #X1_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s1)

                self.model = keras.models.Model([X1_input], embedding_s1)
        meanEmbeddings = AllEmbeddings()
        EMBEDDINGS = EMBEDDINGS*300

jsonfile='BioASQ-training7b.json'
if args.bioasq8b:
    jsonfile='training8b.json'
data = json.load(open(jsonfile, encoding='utf-8'))['questions']
tstnum = "%s" % (int(random.random() * 10000))
if __name__ == "__main__":
    tstnum = ""
    #Diego - run many times with RANDOM seed
    #random.seed(123)
    alldata = list(range(len(data)))
    np.random.seed(args.seed)
    np.random.shuffle(alldata)
    kf = KFold(n_splits=6)
    fold = 0
    for t1,t2 in kf.split(alldata):
        if fold == args.fold:
            train_indices = [alldata[i] for i in t1]
            test_indices = [alldata[i] for i in t2]
        fold += 1
        
    with open("%s_indices.txt" % SAVE_PATH, "w") as f:
        for i in train_indices:
            f.write("%s\n" % (i))
        for i in test_indices:
            f.write("##%s\n" % (i)) #Add "##" to test indicies
    print("All Data: %s" % len(alldata))
    print("Train: %s" % len(train_indices))
    print("Test: %s" % len(test_indices))
    
    #split_boundary = int(len(alldata)*.8)
    #train_indices = alldata[:split_boundary]
    #test_indices = alldata[split_boundary:]

    if args.load == 'BioASQ':
        jsonfile='BioASQ-task8bPhaseB-testset3'
        bioasqdata = json.load(open(jsonfile, encoding='utf-8'))['questions']

    if args.rouge == 'PYTHON':
        rouge_engine = rouge.Rouge()
        print("Using Python ROUGE engine in the RL environment")
    else:
        #perl_scores = perl_scores
        print("Using Perl ROUGE engine in the RL environment")

    if args.observe in ['EMBEDDINGS', 'EMBEDDINGSORIG', 'SUMMARUNNER', 'NEW', 'OLD', 'ALL']:
        setup_embeddings()
    if args.observe in ['BERT']:
        from transformers import BertTokenizer
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        #tokenizer = BertTokenizer.from_pretrained('emilyalsentzer/Bio_ClinicalBERT')
        BERT_EMBEDDINGS = 200
        EMBEDDINGS = 200
        bert_dict = {}
        def text_to_indices(data, limit=SENTENCE_LENGTH):
            "Convert the text to indices and pad-truncate to the maximum number of words"
            return tokenizer.encode(data,add_special_tokens=True, max_length=limit, pad_to_max_length=True)
            #with Pool() as pool:
            #    return pool.map(partial(tokenizer.encode,
            #            add_special_tokens=True, max_length=limit, pad_to_max_length=True), data)
        class BertEmbeddings():
            def __init__(self):
                ###X1 Sentence (All Text)
                X1_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X') #,dtype=np.int32
                #embedding_s1 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS,trainable=True)(X1_input)
                if args.policy != 'BERT':
                    from transformers import BertModel
                    import torch
                    self.model = BertModel.from_pretrained('bert-base-uncased')
                    self.model.eval()
            def traintest(self,text):
                if args.policy == 'BERT':
                    output = np.array([text_to_indices(text,SENTENCE_LENGTH)])
                else:
                    #input = np.array([text_to_indices([text],SENTENCE_LENGTH)])
                    if text in bert_dict.keys():
                        output = bert_dict[text]
                    else:
                        input = torch.tensor(tokenizer.encode(text, add_special_tokens=True, max_length=SENTENCE_LENGTH, pad_to_max_length=True)).unsqueeze(0)
                        input = self.model(input)[0].detach().numpy() #.squeeze(0)
                        output = np.mean(input,axis=1)
                        bert_dict[text] = output
                return output
        if args.policy == 'BERT':
            EMBEDDINGS = SENTENCE_LENGTH
        else:
            import torch
            EMBEDDINGS = 768 #size of bert model output
        meanEmbeddings = BertEmbeddings()

def one_sentence_to_ids(sentence, sentence_length=SENTENCE_LENGTH):
    """Convert one sentence to a list of word IDs."

Crop or pad to 0 the sentences to ensure equal length if necessary.
Words without ID are assigned ID 1.
>>> one_sentence_to_ids(['my','first','sentence'], 2)
([11095, 121], 2)
>>> one_sentence_to_ids(['my','ssecond','sentence'], 2)
([11095, 1], 2)
>>> one_sentence_to_ids(['yes'], 2)
([21402, 0], 1)
"""
    vectordb = sqlite3.connect(DB)
    c = vectordb.cursor()
    word_ids = []
    for w in sentence:
        if len(word_ids) >= sentence_length:
            break
        #c.execute("""SELECT rowid, word
        #          FROM vectors
        #          WHERE word=?""", (w, ))
        c.execute("""SELECT word_index, word
                  FROM vectors
                  INDEXED BY word_idx
                  WHERE word=?""", (w, ))
        r = c.fetchall()
        if len(r) > 0:
            #word_ids.append(r[0][0] - 1) #rowid is 1+ word_index...
            word_ids.append(r[0][0])
        else:
            word_ids.append(1)
    # Pad with zeros if necessary
    num_words = len(word_ids)
    if num_words < sentence_length:
        word_ids += [0]*(sentence_length-num_words)
    vectordb.close()
    return word_ids #, num_words

def yield_candidate_text(questiondata, snippets_only=True):
    """Yield all candidate text for a question
    >>> data = json.load(open("BioASQ-trainingDataset6b.json", encoding='utf-8'))['questions']
    >>> y = yield_candidate_text(data[0], snippets_only=False)
    >>> next(y)
    ('15829955', 0, 'The identification of common variants that contribute to the genesis of human inherited disorders remains a significant challenge.')
    >>> next(y)
    ('15829955', 1, 'Hirschsprung disease (HSCR) is a multifactorial, non-mendelian disorder in which rare high-penetrance coding sequence mutations in the receptor tyrosine kinase RET contribute to risk in combination with mutations at other genes.')
    >>> y = yield_candidate_text(data[1], snippets_only=True)
    >>> next(y)
    ('55046d5ff8aee20f27000007', 0, 'the epidermal growth factor receptor (EGFR) ligands, such as epidermal growth factor (EGF) and amphiregulin (AREG)')
    >>> next(y)
    ('55046d5ff8aee20f27000007', 1, ' EGFR ligands epidermal growth factor (EGF), amphiregulin (AREG) and transforming growth factor alpha (TGFα)')
"""
    past_pubmed = set()
    sn_i = 0
    if 'snippets' not in questiondata:
        return
    for sn in questiondata['snippets']:
        if snippets_only:
            for s in sent_tokenize(sn['text']):
                yield (questiondata['id'], sn_i, s)
                sn_i += 1
            continue

        pubmed_id = os.path.basename(sn['document'])
        if pubmed_id in past_pubmed:
            continue
        past_pubmed.add(pubmed_id)
        file_name = os.path.join("Task5bPubMed", pubmed_id+".xml")
        sent_i = 0
        for s in sent_tokenize(getAbstract(file_name, version="0")[0]):
            yield (pubmed_id, sent_i, s)
            sent_i += 1

if __name__ == "__main__":
    if args.observe in ['TFIDF', 'TFIDFORIG', 'SUMMARUNNERTFIDF']: #'NEW',
        from sklearn.feature_extraction.text import TfidfVectorizer
        from my_tokenizer import my_tokenize    
        import pickle
        TFIDF_FILENAME = "%s_tfidf.pickle" % SAVE_PATH
        #if RESTORE:
        #    with open(TFIDF_FILENAME,'rb') as f:
        #            tfidf = pickle.load(f)
        #else:
        if VERBOSE > 0:
            print("Training tf.idf")
        tfidf_train_text = [data[x]['body'] for x in train_indices]
        tfidf_train_text += [c[2] for x in train_indices for c in yield_candidate_text(data[x], snippets_only=True)]
        ideal_summaries_sentences = []
        for x in train_indices:
            try:
                ideal_summaries = data[x]['ideal_answer']
            except:
                ideal_summaries = ""
            if type(ideal_summaries) != list:
                ideal_summaries = [ideal_summaries]
            for ideal_sum in ideal_summaries:
                ideal_summaries_sentences += sent_tokenize(ideal_sum)
        tfidf_train_text += ideal_summaries_sentences
        #print(len(tfidf_train_text))
        #print(tfidf_train_text[:10])
        tfidf = TfidfVectorizer(tokenizer=my_tokenize)
        tfidf.fit(tfidf_train_text)
        with open(TFIDF_FILENAME,'wb') as f:
            pickle.dump(tfidf,f)
        TFIDF_SIZE = len(tfidf.get_feature_names())


#copied code from diego's classification
def rouge_to_labels(rougeFile, labels, threshold, metric=["SU4"]):
    """Convert ROUGE values into classification labels
    >>> labels = rouge_to_labels("rouge_6b.csv", "topn", 3)
    Setting top 3 classification labels
    >>> labels[(0, '55031181e9bde69634000014', 9)]
    False
    >>> labels[(0, '55031181e9bde69634000014', 20)]
    True
    >>> labels[(0, '55031181e9bde69634000014', 3)]
    True
    """
    assert labels in ["topn", "threshold"]

    # Collect ROUGE values
    rouge = dict()
    with codecs.open(rougeFile,'r','utf-8') as f:
        reader = csv.reader(f)
        header = next(reader)
        index = [header.index(m) for m in metric]
        for line in reader:
            try:
                key = (int(line[0]),line[1],int(line[2]))
            except:
                print("Unexpected error:", sys.exc_info()[0])
                print([l.encode('utf-8') for l in line])
            else:
                rouge[key] = np.mean([float(line[i]) for i in index])

    # Convert ROUGE values into classification labels
    result = dict()
    if labels == "threshold":
        print("Setting classification labels with threshold", threshold)
        for key, value in rouge.items():
            result[key] = (value >= threshold)
        qids = set(k[0] for k in rouge)
        # Regardless of threshold, set top ROUGE of every question to True
        for qid in qids:
            qid_items = [(key, value) for key, value in rouge.items() if key[0] == qid]
            qid_items.sort(key = lambda k: k[1])
            result[qid_items[-1][0]] = True
    else:
        print("Setting top", threshold, "classification labels")
        qids = set(k[0] for k in rouge)
        for qid in qids:
            qid_items = [(key, value) for key, value in rouge.items() if key[0] == qid]
            qid_items.sort(key = lambda k: k[1])
            for k, v in qid_items[-threshold:]:
                result[k] = True
            for k, v in qid_items[:-threshold]:
                result[k] = False
    return result



class BioASQEnv(gym.Env):
    """A BioASQ environment for OpenAI gym"""
    metadata = {'render.modes': ['human']}

    def __init__(self):
        super(BioASQEnv, self).__init__()

        if args.baseline:
            self.reward_range = (-1, 1)
        else:
            self.reward_range = (0, 1)

        if args.continuous_action:
            self.action_space = spaces.Box(low=np.array([0]), high=np.array([1]), dtype=np.float32)
        else:
            self.action_space = spaces.Discrete(2)

        if args.observe == 'SUMMARUNNER':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(3*EMBEDDINGS+5,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'SUMMARUNNERTFIDF':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(3*TFIDF_SIZE+5,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'TFIDF':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(5*TFIDF_SIZE+1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'EMBEDDINGS':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(5*EMBEDDINGS+1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'BERT':
            self.observation_space = spaces.Box(low=0, high=tokenizer.vocab_size, shape=(5*EMBEDDINGS+1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'ALL':
            #if args.policy == "LSTM":
            #    #self.observation_space = spaces.Box(low=-50, high=50, shape=(5*EMBEDDINGS,), dtype=np.float32) #shape=(12,12)
            #    #self.observation_space = spaces.Box(low=-50, high=50, shape=(5,EMBEDDINGS,), dtype=np.float16) #shape=(12,12)
            #    self.observation_space = spaces.Box(low=-50, high=50, shape=(2,EMBEDDINGS,), dtype=np.float32) #shape=(12,12)
            #else:
            self.observation_space = spaces.Box(low=-50, high=50, shape=(5*EMBEDDINGS+1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'NEW':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(5*EMBEDDINGS+5,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'OLD':
            self.observation_space = spaces.Box(low=-50, high=50, shape=(2*EMBEDDINGS+1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'SUMMARY':
            self.observation_space = spaces.Box(low=0, high=1, shape=(1,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'SUMNEW':
            self.observation_space = spaces.Box(low=0, high=1, shape=(2,), dtype=np.float32) #shape=(12,12)
        elif args.observe == 'INDEX':
            self.observation_space = spaces.Box(low=0, high=1, shape=(21,), dtype=np.float32) #shape=(12,12)

    def _next_observation(self):
        if args.observe in ['TFIDF', 'TFIDFORIG']:
            if self.index < len(self.candidates):
                if not self.inputs_saved:
                    self.all_text = tfidf.transform([" ".join(self.candidates)]).todense()[0,:]
                    self.tfidf_all_candidates = tfidf.transform(self.candidates)
                    self.inputs_saved = True
                obs = np.reshape((np.hstack([
                    self.tfidf_all_candidates[self.index].todense(),
                    tfidf.transform([" ".join(self.candidates[self.index + 1:])]).todense()[0, :],
                    tfidf.transform([" ".join([self.candidates[x] for x in self.summary if x < len(self.candidates)])]).todense()[0, :],
                    tfidf.transform([self.question]).todense()[0,:],
                    self.all_text,
                    [[len(self.summary)]],
                ])), (5*TFIDF_SIZE + 1,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    tfidf.transform([" "]).todense()[0, :],
                    tfidf.transform([" "]).todense()[0, :],
                    tfidf.transform([" "]).todense()[0, :],
                    tfidf.transform([" "]).todense()[0, :],
                    tfidf.transform([" "]).todense()[0, :],
                    [[0]],
                ])), (5*TFIDF_SIZE + 1,))
        elif args.observe in ['EMBEDDINGS', 'EMBEDDINGSORIG', 'ALL']:
            if self.index < len(self.candidates):
                if not self.inputs_saved:
                    self.all_text = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join(self.candidates))]))
                    self.embeddings_all_candidates = [one_sentence_to_ids(i) for i in self.candidates]
                    self.inputs_saved = True
                embeddings_this_candidate = meanEmbeddings.model.predict(np.array([self.embeddings_all_candidates[self.index]]))
                embeddings_remaining_candidates = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join(self.candidates[self.index + 1:]))]))
                embeddings_summary = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join([self.candidates[x] for x in self.summary if x < len(self.candidates)]))]))
                embeddings_question = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(self.question)]))
                
                if args.observe == 'ALL':
                    obs = np.reshape((np.hstack([ 
                        np.reshape((embeddings_this_candidate),(EMBEDDINGS,)),
                        np.reshape((self.all_text),(EMBEDDINGS,)),
                        np.reshape((embeddings_remaining_candidates),(EMBEDDINGS,)),
                        np.reshape((embeddings_question),(EMBEDDINGS,)),
                        np.reshape((embeddings_summary),(EMBEDDINGS,)),
                        np.reshape([len(self.summary)],(1,)), #length of summaries chosen - not sentences so far...
                    ])), (5*EMBEDDINGS + 1,))
                else:
                    obs = np.reshape((np.hstack([ 
                        embeddings_this_candidate,
                        self.all_text,
                        embeddings_remaining_candidates,
                        embeddings_question,
                        embeddings_summary,
                        [[len(self.summary)]], #length of summaries chosen - not sentences so far...
                    ])), (5*EMBEDDINGS + 1,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0],
                ])), (5*EMBEDDINGS + 1,))
        elif args.observe == 'NEW':
            if self.index < len(self.candidates):
                if not self.inputs_saved:
                    self.all_text = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join(self.candidates))]))
                    self.embeddings_all_candidates = [one_sentence_to_ids(i) for i in self.candidates]
                    self.inputs_saved = True
                embeddings_this_candidate = meanEmbeddings.model.predict(np.array([self.embeddings_all_candidates[self.index]]))
                embeddings_remaining_candidates = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join(self.candidates[self.index + 1:]))]))
                embeddings_summary = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join([self.candidates[x] for x in self.summary if x < len(self.candidates)]))]))
                embeddings_question = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(self.question)]))

                #get current question type info
                questionnum = {"summary": 1, "list": 2, "factoid": 3, "yesno": 4}
                question_type = questionnum[self.qtype]
                question_embeddings = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(self.question)]))

                obs = np.reshape((np.hstack([ 
                    embeddings_this_candidate,
                    self.all_text,
                    embeddings_remaining_candidates,
                    embeddings_question,
                    embeddings_summary,
                    [[self.index] +                         #4 abs_pos = sentence index
                     [len(self.candidates)] +               #end of document    ##Removed- Inverse index (index compared to end of document)
                     [self.index / len(self.candidates)] +  #5 rel_pos = percent of document up to index
                     [len(self.summary)] +                  #length of summary so far (used in our version!)
                     [question_type]],                      #question type (New for Question Answering!)
                ])), (5*EMBEDDINGS + 5,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*5,
                ])), (5*EMBEDDINGS + 5,))
        elif args.observe in ['BERT']:
            if self.index < len(self.candidates):
                if not self.inputs_saved:
                    self.all_text = meanEmbeddings.traintest(" ".join(self.candidates))
                    self.inputs_saved = True
                embeddings_this_candidate = meanEmbeddings.traintest(self.candidates[self.index])
                embeddings_remaining_candidates = meanEmbeddings.traintest(" ".join(self.candidates[self.index + 1:]))
                embeddings_summary = meanEmbeddings.traintest(" ".join([self.candidates[x] for x in self.summary if x < len(self.candidates)]))
                embeddings_question = meanEmbeddings.traintest(self.question)
                
                obs = np.reshape((np.hstack([ 
                    embeddings_this_candidate,
                    self.all_text,
                    embeddings_remaining_candidates,
                    embeddings_question,
                    embeddings_summary,
                    [[len(self.summary)]], #length of summaries chosen - not sentences so far...
                ])), (5*EMBEDDINGS + 1,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0],
                ])), (5*EMBEDDINGS + 1,))
        elif args.observe == 'OLD':
            if self.index < len(self.candidates):
                embeddings_all_candidates = [one_sentence_to_ids(i) for i in self.candidates]
                embeddings_this_candidate = meanEmbeddings.model.predict(np.array([embeddings_all_candidates[self.index]]))
                embeddings_question = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(self.question)]))

                obs = np.reshape((np.hstack([ 
                    embeddings_this_candidate, #X
                    embeddings_question,       #Q
                    [[self.index]],            #position
                ])), (2*EMBEDDINGS + 1,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0],
                ])), (2*EMBEDDINGS + 1,))
        elif args.observe == 'SUMMARUNNER':
            if self.index < len(self.candidates):
                embeddings_all_candidates = [one_sentence_to_ids(i) for i in self.candidates]
                content = meanEmbeddings.model.predict(np.array([embeddings_all_candidates[self.index]]))
                salience = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(" ".join(self.candidates))])) #Not sure so I'll use the whole document (should be tanh/rouge though?)
                summary_text = ' '.join([self.candidates[s] for s in self.summary])
                try:
                    redundancy = perl_scores([summary_text], self.candidates[self.index], xml_rouge_filename=('%s_rouge.xml' % SAVE_PATH))['SU4'] # = tanh of current sentence compared to summary so far!
                except:
                    redundancy = 0
                absolute_position = self.index
                relative_position = self.index / len(self.candidates) # percent through document
                bias = 0
                #question answering
                questionnum = {"summary": 1, "list": 2, "factoid": 3, "yesno": 4}
                question_type = questionnum[self.qtype]
                question_embeddings = meanEmbeddings.model.predict(np.array([one_sentence_to_ids(self.question)]))
                
                obs = np.reshape((np.hstack([ 
                    content,                #1 content = sentence embeddings
                    salience,               #2 salience (I used document embeddings, not sure if this is right?)
                    [[-redundancy] +        #3 novelty of sentence compared to summary so far
                     [absolute_position] +  #4 abs_pos = sentence index
                     [relative_position] +  #5 rel_pos = percent of document up to index
                     [len(self.summary)] +  #length of summary so far (used in our version!)
                     [question_type]],      #question type (New for Question Answering!)
                    question_embeddings,    #question embeddings (used in ours for Question Answering!)
                ])), (3*EMBEDDINGS+5,))
            elif self.done == 1:
                obs = np.reshape((np.hstack([
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*EMBEDDINGS,
                    [0]*5,
                ])), (3*EMBEDDINGS+5,))
        elif args.observe == 'SUMMARY':
            obs = np.array([ 
                len(self.summary),
            ])
        elif args.observe == 'SUMNEW':
            obs = np.array([ 
                self.index, 
                len(self.summary),
            ])
        elif args.observe == 'INDEX':
            obs = np.array([ 
                self.index / 30,
                0 in self.summary,
                1 in self.summary,
                2 in self.summary,
                3 in self.summary,
                4 in self.summary,
                5 in self.summary,
                6 in self.summary,
                7 in self.summary,
                8 in self.summary,
                9 in self.summary,
                10 in self.summary,
                11 in self.summary,
                12 in self.summary,
                13 in self.summary,
                14 in self.summary,
                15 in self.summary,
                16 in self.summary,
                17 in self.summary,
                18 in self.summary,
                19 in self.summary,
            ])
        return obs #observations - maybe the word embeddings as well!!!

    def _take_action(self, _action):
        #print("a: %s" % _action)
        if _action >= 0.5:
            action = 1
        else:
            action = 0
        assert action in self.actions
        assert self.index < len(self.candidates)

        if action == 0:
            self.summary.append(self.index)

        ###########returns reward!!!
        self.index += 1
        self.rouge = 0
        done = self.index >= len(self.candidates)
        if done or args.early_reward:
            if len(self.summary) == 0:
                print("Warning: empty summary has been produced; reward = 0")
                self.rouge = 0
            elif len(self.ideal_summaries) == 0:
                print("Warning: empty target summaries; reward = 0")
                self.rouge = 0
            else:
                summary_text = ' '.join([self.candidates[s] for s in self.summary])
                try:
                    if args.rouge == 'PYTHON':
                        rouge_scores = [rouge_engine.get_scores(h, summary_text)[0] for h in self.ideal_summaries]
                        #print(rouge_scores)
                        rouge_score = max([(r['rouge-2']['f'] + r['rouge-l']['f'])/2 for r in rouge_scores])
                    else:
                        rouge_scores = perl_scores(self.ideal_summaries, summary_text, 
                                                    xml_rouge_filename=('%s%s_rouge.xml' % (SAVE_PATH, 'train' if self.qid in train_indices else ('test' + tstnum))))
                        rouge_score = rouge_scores['SU4']
                except Exception as err:
                    print("ERROR in QID %i:" % (self.qid), err)
                    #traceback.print_exc() #RAIJIN stops if/when the error is printed!
                    rouge_score = 0
                self.rouge = rouge_score

        #logging
        if done:
            if self.qid in train_indices:
                try:
                    with open("%s_train.txt" % SAVE_PATH, "a") as f:
                        f.write("%s\n" % (self.rouge))
                except:
                    pass
            else:
                if VERBOSE > 0:
                    #print("Test Reward %s [%s]" % (self.rouge, ", ".join(["%0.1f" % i for i in self._log])))
                    print("Test Reward %s [%s]" % (self.rouge, ", ".join(["%0.1f" % i for i in self._log])))
                try:
                    with open("%s_testsents.txt" % SAVE_PATH, "a") as f:
                        #f.write("%s: %s\n" % ( ", ".join(["%s (%0.2f)" % ("1" if i >= 0.5 else "0",i) for i in self._log]),self.rouge))
                        f.write("%s (%s/%s): %s\n" % (self.rouge, sum([0 if i >= 0.5 else 1 for i in self._log]), len(self.candidates), ", ".join(["%s" % i for i in self._log])))
                except:
                    pass
        else:
            if self.qid in train_indices:
                try:
                    with open("%s_train.txt" % SAVE_PATH, "a") as f:
                        f.write("x\n")
                except:
                    pass

    def step(self, action):
        # Execute one time step within the environment
        self._take_action(action)

        #self.current_step self.index
        #delay_modifier = (self.current_step / MAX_STEPS) #not needed?
        reward = self.rouge - self.baseline #* delay_modifier
        self.done = 1 if self.index >= len(self.candidates) else 0
        obs = self._next_observation()

        return obs, reward, self.done, {}

    def reset(self,qid=None,q=None):
        self._log = []
        if q == None:
            if qid == None:
                qid = np.random.choice(train_indices)
            if VERBOSE > 0:
                print("Resetting environment to query ID %i" % qid)
            self.qid = qid
            self.id = data[qid]['id']
            self.qtype = data[qid]['type']
            self.question = data[qid]['body']
            self.candidates = [s[2] for s in yield_candidate_text(data[qid])]
            self.candidates = self.candidates[:20] # TODO: Remove the limit to first 20 candidates
            try:
                self.ideal_summaries = data[qid]['ideal_answer']
            except:
                self.ideal_summaries = "" #3157 - no ideal answer...
            if type(self.ideal_summaries) != list:
                self.ideal_summaries = [self.ideal_summaries]
            self.summary = list()
            self.actions = (0, 1)
            self.index = 0
            self.inputs_saved = False
            self.baseline = 0
            self.calculate_baseline()
        else:
            if VERBOSE > 0:
                print("Resetting environment to answer BioASQ question")
            self.qid = 0
            self.id = q['id']
            self.qtype = q['type']
            self.question = q['body']
            self.candidates = [s[2] for s in yield_candidate_text(q)]
            self.candidates = self.candidates[:20] # TODO: Remove the limit to first 20 candidates
            self.ideal_summaries = []  #NONE!!!
            self.summary = list()
            self.actions = (0, 1)
            self.index = 0
            self.inputs_saved = False
            self.baseline = 0
        if args.reverse:
            self.candidates = self.candidates[::-1]

        return self._next_observation()

    def calculate_baseline(self):
        if not args.baseline:
            self.baseline = 0
        elif self.qid == 0 or self.qid in test_indices:
            self.baseline = 0
        elif self.qid in train_indices:
            ###########returns reward for firstn!!!
            nanswers = {"summary": 6, "factoid": 2, "yesno": 2, "list": 3}
            topn_summary = [i for i in range(len(self.candidates)) if i <= nanswers[self.qtype]]
            if len(topn_summary) == 0:
                #print("Warning: empty summary has been produced; reward = 0")
                self.baseline = 0
            elif len(self.ideal_summaries) == 0:
                #print("Warning: empty target summaries; reward = 0")
                self.baseline = 0
            else:
                summary_text = ' '.join([self.candidates[s] for s in topn_summary])
                try:
                    if args.rouge == 'PYTHON':
                        rouge_scores = [rouge_engine.get_scores(h, summary_text)[0] for h in self.ideal_summaries]
                        #print(rouge_scores)
                        rouge_score = max([(r['rouge-2']['f'] + r['rouge-l']['f'])/2 for r in rouge_scores])
                    else:
                        rouge_scores = perl_scores(self.ideal_summaries, summary_text, 
                                                    xml_rouge_filename=('%s%s_rouge.xml' % (SAVE_PATH, 'train' if self.qid in train_indices else ('test' + tstnum))))
                        rouge_score = rouge_scores['SU4']
                except Exception as err:
                    print("ERROR in QID %i:" % (self.qid), err)
                    #traceback.print_exc() #RAIJIN stops if/when the error is printed!
                    rouge_score = 0
                self.baseline = rouge_score

    def render(self, mode='human', close=False):
        # Render the environment to the screen
        #profit = 0
        if self.index >= len(self.candidates) - 1:
            print(f'ID: {self.qid}; Summaries: {self.summary}')
        elif self.rouge > 0:
            print(f'Rouge: {self.rouge}')

    def test_reward(self,model,indicies,deterministic=False):
        all_rewards = []
        if args.observe == 'BERT' and args.policy != 'BERT':
            try:
                meanEmbeddings.model.save("BERT_EMBEDDINGS")
            except:
                pass
        #Test Data:
        for i in indicies:
            obs = self.reset(i)
            done = False
            rewards = 0
            _states = None
            while done == False:
                if 'REINFORCE' in args.policy or args.model == 'CLASSIFICATION':
                    if "TFIDF" in args.observe:
                        output_val = model.model_predict.predict(obs)[0][0]
                    else:
                        output_val = model.model_predict.predict(np.array([obs]))[0][0]
                    #print(output_val)
                    self._log.append(output_val)
                    _action = 0
                    if output_val >= 0.5:
                        _action = 1
                elif args.policy in ['MLPLSTM200', 'CNNLSTM200', 'MLPLSTMLN200']:
                    _action, _states = model.predict([obs]*args.cpus)
                    _action = _action[0]
                    self._log.append(_action)
                elif args.model == 'REINFORCE':
                    _action = model.step(obs)[0]
                    self._log.append(_action)
                else:
                    if args.bestaction == True:
                        probability = 0
                        for loop in range(100):
                            _a, _s = model.predict(obs,mask=[done],deterministic=deterministic)
                            if _a > 0.5:
                                probability += 1
                        _action = 0
                        if probability > 90: #50
                            _action = 1
                        self._log.append(probability/100)
                    else:
                        _action, _states = model.predict(obs,state=_states,mask=[done],deterministic=deterministic)
                        if args.policy in ['CUSTNN', 'CUSTNNLOAD', 'CUSTNNCPU']:
                            self._log.append(model.act_model.outl[0])
                        else:
                            self._log.append(_action)
                    
                obs, rewards, done, info = self.step(_action)
                #env.render()
            all_rewards.append(rewards)
        self.reset()
        return all_rewards

    def answers(self,model,questions,deterministic=False):
        out_file = []
        #Test Data:
        for i in questions:
            obs = self.reset(0,i)
            if len(self.candidates) == 0:
                print("WARNING - No candidate inputs!")
                try:
                    with open("%s_testsents.txt" % SAVE_PATH, "a") as f:
                        f.write("%s (%s/%s): %s\n" % (self.id, 0, 0, "X"))
                except:
                    pass
                out_file.append({"id": self.id,
                                 "ideal_answer": "",
                                 "exact_answer": "yes" if self.qtype == "yesno" else ""})
            else:
                done = False
                rewards = 0
                _states = None
                while done == False:
                    if 'REINFORCE' in args.policy or args.model == 'CLASSIFICATION':
                        if "TFIDF" in args.observe:
                            output_val = model.model_predict.predict(obs)[0][0]
                        else:
                            output_val = model.model_predict.predict(np.array([obs]))[0][0]
                        #print(output_val)
                        _action = 0
                        if output_val >= 0.5:
                            _action = 1
                    elif args.policy in ['MLPLSTM200', 'CNNLSTM200', 'MLPLSTMLN200']:
                        _action, _states = model.predict([obs]*args.cpus)
                        _action = _action[0]
                    elif args.model == 'REINFORCE':
                        _action = model.step(obs)[0]
                    else:
                        if args.bestaction == True:
                            probability = 0
                            for loop in range(100):
                                _a, _s = model.predict(obs,mask=[done],deterministic=deterministic)
                                if _a > 0.5:
                                    probability += 1
                            _action = 0
                            #50: 0.2558 , 70: 0.2577 , 80: 0.2642 , 90: 0.2651
                            if probability > 90:
                                _action = 1
                            self._log.append(probability/100)
                        else:
                            _action, _states = model.predict(obs,state=_states,mask=[done],deterministic=deterministic)
                    last_id = self.id
                    last_type = self.qtype
                    last_log = self._log
                    obs, rewards, done, info = self.step(_action)
                    if len(self.summary) == 0:
                        last_answers = [self.candidates[0]]
                        #last_answers = [self.candidates[last_log.index(max(last_log))]]
                    else:
                        last_answers = [self.candidates[i] for i in self.summary]
                try:
                    with open("%s_testsents.txt" % SAVE_PATH, "a") as f:
                        f.write("%s (%s/%s): %s\n" % (last_id, sum([0 if i >= 0.5 else 1 for i in last_log]), len(last_log), ", ".join(["%s" % i for i in last_log])))
                except:
                    pass
                out_file.append({"id": last_id,
                                 "ideal_answer": " ".join(last_answers),
                                 "exact_answer": "yes" if last_type == "yesno" else ""})
        self.reset()
        return out_file

best, n_steps = -500, 0

def get_reward_firstn(indicies, _random=False,all=False, last=False):
    nanswers = {"summary": 6,
                "factoid": 2,
                "yesno": 2,
                "list": 3}
    all_rewards = []
    #Test Data:
    for i in indicies:
        obs = env.envs[0].reset(i)
        done = False
        rewards = 0
        n = 0
        while done == False:
            if _random:
                action = [0 if random.random() < 0.5 else 1,]
            elif all:
                action = [0,]
            elif last:
                action = [0 if (len(env.envs[0].candidates) - n) <= nanswers[env.envs[0].qtype] else 1,]
            else:
                action = [0 if n < nanswers[env.envs[0].qtype] else 1,]
            states = env.envs[0].summary
            obs, rewards, done, info = env.step(action)
            n = n + 1
        all_rewards.append(rewards)
        print(states, rewards)
    return all_rewards


def callback(_locals, _globals=None):
    #global n_steps, STEPS
    global n_steps, best, SAVE_PATH, STEPS, env, test_model
    if (n_steps + 1) % STEPS == 0:
        if args.thread_test: #multithread (in the future this could load saved model??)
            _locals['self'].save(SAVE_PATH + ("_TEMP"))
            print("Saved TEMP model, starting thread")
            p = Process(target=run_test, args=({"model":args.model, "observe":args.observe, "rouge":args.rouge, "n_steps":n_steps, "best":best, "env_test":env, "SAVE_PATH":SAVE_PATH, "STEPS":STEPS, "train_indices":train_indices, "test_indices":test_indices, "deterministic":(True if args.model == 'REINFORCE' else False)}, True,))
            p.start()
            #p.join()
        else:
            run_test(_locals['self'])
    n_steps += 1
    # Returning False will stop training early
    return True
def run_test(_args=None, multi_thread=False):
    global args
    if not multi_thread:
        global n_steps, best, env, SAVE_PATH, STEPS, test_indices
        #deterministic = (True if args.model in ['REINFORCE', 'PPO5'] else False)
        #deterministic = True
        deterministic = (True if args.policy == 'REINFORCE200' else False)
        test_model = _args
        if best == -500 and args.policy == 'CUSTNNLOAD':
            test_model.act_model.loadmodel()
    else:
        global train_indices, meanEmbeddings
        n_steps = _args["n_steps"]
        best = _args["best"]
        env = _args["env_test"]
        SAVE_PATH = _args["SAVE_PATH"]
        STEPS = _args["STEPS"]
        test_indices = _args["test_indices"]
        train_indices = _args["train_indices"]
        deterministic = _args["deterministic"]
        args.observe = _args["observe"]
        args.rouge = _args["rouge"]
        if args.observe in ['EMBEDDINGS', 'SUMMARUNNER', 'NEW', 'OLD']:
            setup_embeddings(False)
        print("Loading Model")
        if _args["model"] == 'PPO1':
            test_model = PPO1.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'PPO2':
            test_model = PPO2.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'PPO5':
            test_model = PPO5.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'PPO4':
            test_model = PPO4.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'TRPO':
            test_model = TRPO.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'A2C':
            test_model = A2C.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'ACER':
            test_model = ACER.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'ACER2':
            test_model = ACER2.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'DQN':
            test_model = DQN.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'DDPG':
            test_model = DDPG.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'HER':
            test_model = HER.load(SAVE_PATH + "_TEMP", env=env)
        elif _args["model"] == 'SAC':
            test_model = SAC.load(SAVE_PATH + "_TEMP", env=env)
        print("Model Loaded")

    #predict:
    print("Predicting")
    test_data = [0]
    if args.observe == "FLAGRUN":
        x, y = ts2xy(load_results("."), 'timesteps')
        test_data = y[-STEPS*(args.batch_size if STEPS <= 50 else 1):]
    else:
        test_data = env.envs[0].test_reward(test_model,test_indices, deterministic=deterministic)
        
    test = np.mean(test_data)
    print("TestData Average: %s" % test)
    
    #save:
    try:
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("%s: %s%s\n" % ((n_steps+1)*(args.batch_size if STEPS <= 50 else 1), test, "" if test < best else "**"))
        with open("%s_testsents.txt" % SAVE_PATH, "a") as f:
            f.write("%s:: %s%s\n" % ((n_steps+1)*(args.batch_size if STEPS <= 50 else 1), test, "" if test < best else "**"))
        if test >= best:
            best = test
            print("Saving Best Model")
            if 'REINFORCE' in args.policy or args.model == 'CLASSIFICATION':
                test_model.model_predict.save(SAVE_PATH + ".ckpt")
            else:
                test_model.save(SAVE_PATH)
                
        with open("%s_testdata.txt" % SAVE_PATH, "a") as f:
            for i in test_data:
                f.write("%s\n" % i)
    except:
        print("Error Saving - Continue!...")

class NNModel():
    def __init__(self, vocabulary_size, hidden_list):
        X_state = keras.layers.Input(shape=vocabulary_size, name='X')
        reward = keras.layers.Input(shape=(1,))
        hidden = X_state #keras.layers.Concatenate()([X_state, Q_state])
        self.episode = tf.placeholder(tf.float32)
        self.initial_state = None
        
        for n_hidden in hidden_list:
            hidden = keras.layers.Dense(n_hidden, activation="relu")(hidden)
        if args.linear:
            #keras.activations.linear
            outputs = keras.layers.Dense(1, activation=None)(hidden)
        else:
            outputs = keras.layers.Dense(1, activation="sigmoid")(hidden)

        def custom_loss(y_true, y_predict):
            "From https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
            #log_lik = keras.backend.log(y_true * (y_true - y_predict) + (1 - y_true) * (y_true + y_predict))
            loss = - (y_true * keras.backend.log(y_predict + 0.0000001) + (1 - y_true) * keras.backend.log(1 - y_predict + 0.0000001)) # Using the standard cross-entropy formula
            return keras.backend.mean(loss * reward, keepdims=True)

        self.model_train = keras.models.Model([X_state, reward], outputs)
        self.model_train.compile(loss=custom_loss, optimizer=keras.optimizers.Adam(0.001)) #lr = 0.000001
        #0.000001 500: 0.24718681222707423** (No change)
        #0.00001  500:
        #0.0001   500: 0.24080860262008733**


        self.model_classify = keras.models.Model([X_state], outputs)
        self.model_classify.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='binary_crossentropy', metrics=['accuracy'])
        
        
        self.model_regression = keras.models.Model([X_state], outputs)
        self.model_regression.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='mean_squared_error', metrics=['mean_squared_error'])

        self.model_predict = keras.models.Model([X_state], outputs)

#custom policy based on NNModel
class CustomPolicy(ActorCriticPolicy):
    def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=False, **kwargs):
        super(CustomPolicy, self).__init__(sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=reuse, scale=True)

        with tf.variable_scope("model", reuse=reuse):
            _relu = tf.nn.relu
            _sigmoid = tf.nn.sigmoid

            #extracted_features = nature_cnn(self.processed_obs, **kwargs)
            extracted_features = tf.layers.flatten(self.processed_obs)

            pi_h = extracted_features
            #for i, layer_size in enumerate([200, 128, 128]):
            pi_h = _relu(tf.layers.dense(pi_h, 200, name='pi_fc1'))
            pi_h = _relu(tf.layers.dense(pi_h, 200, name='pi_fc2'))
            #pi_h = _sigmoid(tf.layers.dense(pi_h, 1, name='pi_fc3'))
            pi_latent = pi_h

            #vf_h = extracted_features
            #for i, layer_size in enumerate([32, 32]):
            #vf_h = _relu(tf.layers.dense(vf_h, 200, name='vf_fc1'))
            #vf_h = _relu(tf.layers.dense(vf_h, 200, name='vf_fc2'))
            value_fn =  pi_latent #_sigmoid(tf.layers.dense(vf_h, 1, name='vf'))
            vf_latent = pi_latent

            self._proba_distribution, self._policy, self.q_value = \
                self.pdtype.proba_distribution_from_latent(pi_latent, vf_latent, init_scale=0.01)

        self._value_fn = value_fn
        self._setup_init()

    def step(self, obs, state=None, mask=None, deterministic=False):
        if deterministic:
            action, value, neglogp = self.sess.run([self.deterministic_action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        else:
            action, value, neglogp = self.sess.run([self.action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        return action, value, self.initial_state, neglogp

    def proba_step(self, obs, state=None, mask=None):
        return self.sess.run(self.policy_proba, {self.obs_ph: obs})

    def value(self, obs, state=None, mask=None):
        return self.sess.run(self.value_flat, {self.obs_ph: obs})


#custom policy based on NNModel - LSTM!
class CustomEMBEDDINGS(ActorCriticPolicy):
    def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=False, **kwargs):
        super(CustomEMBEDDINGS, self).__init__(sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=reuse, scale=False)

        with tf.variable_scope("model", reuse=reuse):
            #P-F
            X_state = self.obs_ph #self.processed_obs
            #Shape(*batches,timesteps,input) = (*None,1,21) < Expand with 1(timestep) in middle
            #https://machinelearningmastery.com/use-timesteps-lstm-networks-time-series-forecasting/
            #A representation with 1 time step would be the default representation when using a stateful LSTM. Using 2 to 5 timesteps is contrived. The hope would be that the additional context from the lagged observations may improve the performance of the predictive model.
            X_state2 = X_state #tf.expand_dims(X_state,1,name=None)
            
            #keras.layers.Bidirectional(keras.layers.LSTM(EMBEDDINGS, dropout=1-keep_prob))(word_embeddings)
            #X_state2 = tf.expand_dims(X_state,1,name=None)
            
            #import pdb
            #pdb.set_trace()
            W1 = X_state2[:,:EMBEDDINGS]
            W2 = X_state2[:,EMBEDDINGS*1:EMBEDDINGS*2]
            W3 = X_state2[:,EMBEDDINGS*2:EMBEDDINGS*3]
            W4 = X_state2[:,EMBEDDINGS*3:EMBEDDINGS*4]
            W5 = X_state2[:,EMBEDDINGS*4:EMBEDDINGS*5]
            W6 = X_state2[:,EMBEDDINGS*5:]
            #EMBEDDINGS
            embed1 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS, trainable=True)(W1)
            pool1 = keras.layers.GlobalAveragePooling1D()(embed1)
            embed2 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS, trainable=True)(W2)
            pool2 = keras.layers.GlobalAveragePooling1D()(embed2)
            embed3 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS, trainable=True)(W3)
            pool3 = keras.layers.GlobalAveragePooling1D()(embed3)
            embed4 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS, trainable=True)(W4)
            pool4 = keras.layers.GlobalAveragePooling1D()(embed4)
            embed5 = keras.layers.Embedding(tokenizer.vocab_size, BERT_EMBEDDINGS, trainable=True)(W5)
            pool5 = keras.layers.GlobalAveragePooling1D()(embed5)
            out6 = W6
            #out6 = W6 #tf.expand_dims(W6,1,name=None)
            
            lstm = keras.layers.Concatenate(axis=-1)([pool1,pool2,pool3,pool4,pool5,out6])
            
            
            #lstm = keras.layers.Bidirectional(keras.layers.LSTM(200))(X_state2)
            
            #hidden = keras.layers.Dense(200, activation="relu",name='hidden')(X_state) #OK!!
            hidden = keras.layers.Dense(200, activation="relu",name='hidden')(lstm)
            hidden2 = keras.layers.Dense(200, activation="relu",name='hidden2')(hidden)
            #outputs = keras.layers.Dense(1, activation="sigmoid",name='outp')(hidden2)
            outputs = hidden2
            #concat = keras.layers.Concatenate()([outputs, hidden2])
            concat = outputs
            self.output = outputs

            #For PPO - use tensorflow model outputs (same model)
            pi_latent = concat #hidden2
            vf_latent = hidden2
            value_fn  = hidden2
            self._proba_distribution, self._policy, self.q_value = self.pdtype.proba_distribution_from_latent(pi_latent, vf_latent, init_scale=0.01)
        self._value_fn = value_fn
        self._setup_init()
        
    def step(self, obs, state=None, mask=None, deterministic=False):
        if deterministic:
            action, value, neglogp = self.sess.run([self.deterministic_action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        else:
            action, value, neglogp = self.sess.run([self.action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        return action, value, self.initial_state, neglogp

    def proba_step(self, obs, state=None, mask=None):
        return self.sess.run(self.policy_proba, {self.obs_ph: obs})

    def value(self, obs, state=None, mask=None):
        return self.sess.run(self.value_flat, {self.obs_ph: obs})


#custom policy based on NNModel - LSTM!
class CustomLSTM(ActorCriticPolicy):
    def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=False, **kwargs):
        super(CustomLSTM, self).__init__(sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=reuse, scale=True)

        with tf.variable_scope("model", reuse=reuse):
            #P-F
            X_state = self.obs_ph #self.processed_obs
            #Shape(*batches,timesteps,input) = (*None,1,21) < Expand with 1(timestep) in middle
            #https://machinelearningmastery.com/use-timesteps-lstm-networks-time-series-forecasting/
            #A representation with 1 time step would be the default representation when using a stateful LSTM. Using 2 to 5 timesteps is contrived. The hope would be that the additional context from the lagged observations may improve the performance of the predictive model.
            X_state2 = tf.expand_dims(X_state,1,name=None)
            
            #keras.layers.Bidirectional(keras.layers.LSTM(EMBEDDINGS, dropout=1-keep_prob))(word_embeddings)
            #X_state2 = tf.expand_dims(X_state,1,name=None)
            
            #import pdb
            #pdb.set_trace()
            W1 = X_state2[:,:,:EMBEDDINGS]
            W2 = X_state2[:,:,EMBEDDINGS*1:EMBEDDINGS*2]
            W3 = X_state2[:,:,EMBEDDINGS*2:EMBEDDINGS*3]
            W4 = X_state2[:,:,EMBEDDINGS*3:EMBEDDINGS*4]
            W5 = X_state2[:,:,EMBEDDINGS*4:EMBEDDINGS*5]
            W6 = X_state2[:,:,EMBEDDINGS*5:]
            #EMBEDDINGS
            lstm1 = keras.layers.Bidirectional(keras.layers.LSTM(200))(W1)
            lstm2 = keras.layers.Bidirectional(keras.layers.LSTM(200))(W2)
            lstm3 = keras.layers.Bidirectional(keras.layers.LSTM(200))(W3)
            lstm4 = keras.layers.Bidirectional(keras.layers.LSTM(200))(W4)
            lstm5 = keras.layers.Bidirectional(keras.layers.LSTM(200))(W5)
            lstm6 = keras.backend.squeeze(W6,axis=1)
            
            lstm = keras.layers.Concatenate(axis=-1)([lstm1,lstm2,lstm3,lstm4,lstm5,lstm6])
            
            
            #lstm = keras.layers.Bidirectional(keras.layers.LSTM(200))(X_state2)
            
            #hidden = keras.layers.Dense(200, activation="relu",name='hidden')(X_state) #OK!!
            hidden = keras.layers.Dense(200, activation="relu",name='hidden')(lstm)
            hidden2 = keras.layers.Dense(200, activation="relu",name='hidden2')(hidden)
            #outputs = keras.layers.Dense(1, activation="sigmoid",name='outp')(hidden2)
            outputs = hidden2
            #concat = keras.layers.Concatenate()([outputs, hidden2])
            concat = outputs
            self.output = outputs

            #For PPO - use tensorflow model outputs (same model)
            pi_latent = concat #hidden2
            vf_latent = hidden2
            value_fn  = hidden2
            self._proba_distribution, self._policy, self.q_value = self.pdtype.proba_distribution_from_latent(pi_latent, vf_latent, init_scale=0.01)
        self._value_fn = value_fn
        self._setup_init()
        
    def step(self, obs, state=None, mask=None, deterministic=False):
        if deterministic:
            action, value, neglogp = self.sess.run([self.deterministic_action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        else:
            action, value, neglogp = self.sess.run([self.action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        return action, value, self.initial_state, neglogp

    def proba_step(self, obs, state=None, mask=None):
        return self.sess.run(self.policy_proba, {self.obs_ph: obs})

    def value(self, obs, state=None, mask=None):
        return self.sess.run(self.value_flat, {self.obs_ph: obs})

#custom policy based on NNModel - import CLASSIFICATION weights and run PPO!
class CustomNN(ActorCriticPolicy):
    def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=False, **kwargs):
        super(CustomNN, self).__init__(sess, ob_space, ac_space, n_env, n_steps, n_batch, reuse=reuse, scale=True)

        with tf.variable_scope("model", reuse=reuse):
            #P-F
            X_state = self.obs_ph #self.processed_obs
            hidden = keras.layers.Dense(200, activation="relu",name='hidden')(X_state)
            hidden2 = keras.layers.Dense(200, activation="relu",name='hidden2')(hidden)
            outputs = keras.layers.Dense(1, activation="sigmoid",name='outp')(hidden2)
            #concat = keras.layers.Concatenate()([outputs, hidden2])
            concat = outputs
            self.output = outputs

            #For PPO - use tensorflow model outputs (same model)
            pi_latent = concat #hidden2
            vf_latent = hidden2
            value_fn  = hidden2
            self._proba_distribution, self._policy, self.q_value = self.pdtype.proba_distribution_from_latent(pi_latent, vf_latent, init_scale=0.01)
        self._value_fn = value_fn
        self._setup_init()
        
    
        self.loaded = False
        for tensor in tf.global_variables():
            if tensor.name == "model/hidden/kernel:0":
                self.hidden = tensor
            if tensor.name == "model/hidden2/kernel:0":
                self.hidden2 = tensor
            if tensor.name == "model/hidden3/kernel:0":
                self.hidden3 = tensor
            if tensor.name == "model/hidden4/kernel:0":
                self.hidden4 = tensor
            if tensor.name == "model/outp/kernel:0":
                self.outp = tensor
            if tensor.name == "model/pi/w:0":
                self.pi_w = tensor
            if tensor.name == "model/q/w:0":
                self.q_w = tensor
            if tensor.name == "model/outv/kernel:0":
                self.outv = tensor
            if tensor.name == "model/hidden/bias:0":
                self.hidden_b = tensor
            if tensor.name == "model/hidden2/bias:0":
                self.hidden2_b = tensor
            if tensor.name == "model/hidden3/bias:0":
                self.hidden3_b = tensor
            if tensor.name == "model/hidden4/bias:0":
                self.hidden4_b = tensor
            if tensor.name == "model/outp/bias:0":
                self.outp_b = tensor
            if tensor.name == "model/outv/bias:0":
                self.outv_b = tensor
            if tensor.name == "model/pi/b:0":
                self.pi_b = tensor
            if tensor.name == "model/q/b:0":
                self.q_b = tensor
            

    def loadmodel(self):
        POLICY_WEIGHT = 1
        if args.policy == 'CUSTNNLOAD' and self.loaded == False:
            print(tf.trainable_variables())
            self.loaded = True
            #For CLASSIFICATION - import saved weights
            model = keras.models.load_model(SAVE_PATH + "_LOAD.ckpt")
            model.summary()
            
            print(self.hidden.shape)
            print(np.array(model.layers[-3].get_weights()[0]).shape)
            self.sess.run(self.hidden.assign(np.array(model.layers[-3].get_weights()[0])))
            self.sess.run(self.hidden_b.assign(np.array(model.layers[-3].get_weights()[1])))
            weights = self.sess.run(self.hidden)
            print(weights)
            
            
            print(self.hidden2.shape)
            print(np.array(model.layers[-2].get_weights()[0]).shape)
            self.sess.run(self.hidden2.assign(np.array(model.layers[-2].get_weights()[0])))
            self.sess.run(self.hidden2_b.assign(np.array(model.layers[-2].get_weights()[1])))
            weights = self.sess.run(self.hidden2)
            print(weights)
            
            print(self.outp.shape)
            print(np.array(model.layers[-1].get_weights()[0]).shape)
            self.sess.run(self.outp.assign(model.layers[-1].get_weights()[0]))
            self.sess.run(self.outp_b.assign(model.layers[-1].get_weights()[1]))
            weights = self.sess.run(self.outp)
            
            
            print(np.array(model.layers[-1].get_weights()[0]).shape)
            x = np.zeros(self.pi_w.shape)
            y = np.zeros(self.pi_b.shape)
            x[0,0] = -POLICY_WEIGHT
            x[0,1] = POLICY_WEIGHT
            y[0] = (POLICY_WEIGHT/2)
            y[1] = -(POLICY_WEIGHT/2)
            #-1,1,1: 0.2156967467248908
            #-2,2,1,-1: 0.2330872707423581
            #-10,10,5,-5: 0.247   (no random!)
            self.sess.run(self.pi_w.assign(x))
            self.sess.run(self.pi_b.assign(y))
            weights = self.sess.run(self.pi_w)
            print(weights)
            
            self.sess.run(self.q_w.assign(np.zeros(self.q_w.shape)))
            self.sess.run(self.q_b.assign(np.zeros(self.q_b.shape)))
            
            print("Loaded Weights!!")

    def step(self, obs, state=None, mask=None, deterministic=False):
        self.loadmodel()
        if deterministic:
            action, value, neglogp = self.sess.run([self.deterministic_action, self.value_flat, self.neglogp],
                                                   {self.obs_ph: obs})
        else:
            action, value, neglogp, outl, pl = self.sess.run([self.action, self.value_flat, self.neglogp, self.output, self._policy],{self.obs_ph: obs})
        self.outl = outl
        #print(action,outl,pl,neglogp)
        return action, value, self.initial_state, neglogp

    def proba_step(self, obs, state=None, mask=None):
        return self.sess.run(self.policy_proba, {self.obs_ph: obs})

    def value(self, obs, state=None, mask=None):
        return self.sess.run(self.value_flat, {self.obs_ph: obs})

def reinforce_diego(env, timesteps, callback,nnModel,RESTORE=None):
    # The following code is based on "Policy Gradients"
    # at https://github.com/ageron/handson-ml/blob/master/16_reinforcement_learning.ipynb 

    # Reset to a random question
    observation = env.reset()
    #keep track of states and actions - to train model
    states = []
    actions = []
    _states = None
    done = False

    episode = 0 #keep track of episode
    while episode < timesteps:
        #Predict action based on the model observation
        predict = nnModel.model_predict.predict(observation)[0][0]
        #Generate action probabilities (this is better as it gives 1/3 probability even to 0 values...?)
        perturb = 0.2*timesteps/(timesteps+episode)
        p_left_and_right = [(1-predict+1*perturb)/(1+2*perturb), (predict+1*perturb)/(1+2*perturb)] #[prob 0, prob 1]

        #Randomly choose action based on prediction
        #print(p_left_and_right)
        action_val = np.random.choice(2, p=p_left_and_right)

        #append the state and actions taken at each step
        states.append(observation)
        actions.append(action_val)

        #take the action in the environment and step forward
        observation, rewards, done, info = env.step([action_val,])

        if done:
            print("Episode: %i, reward: %f" % (episode, rewards))
            #update model by training on the actions that weren't taken with the final reward
            loss = nnModel.model_train.train_on_batch([np.vstack(states), np.array([rewards]*len(states))], actions)
            #reset stored states and actions
            states = []
            actions = []

        #Run tests and save checkpoint (every STEPS times)
        callback({'self':nnModel})
        
        if (n_steps) % STEPS == 0:
            #Refresh if the checkpoint was saved
            observation = env.reset()
            states = []
            actions = []
        episode += 1

if __name__ == "__main__":
    # The algorithms require a vectorized environment to run
    if args.policy in ['MLP64', 'MLP200', 'MLP400', 'CNN200', 'CUST200', 'BERT', 'CUSTNN', 'CUSTNNLOAD', 'REINFORCE200', 'MLP1', 'LSTM']:
        if args.observe == 'FLAGRUN':
            #import roboschool
            import pybulletgym
            env = DummyVecEnv([lambda: Monitor(gym.make('HumanoidFlagrunPyBulletEnv-v0'),SAVE_PATH + "l", allow_early_resets=True)])
        else:
            env = DummyVecEnv([lambda: BioASQEnv()])
        #if args.thread_test:
        #    env_test = DummyVecEnv([lambda: BioASQEnv()])
        #    env_test.envs[0].qid = 0
        if args.policy in ['MLP200', 'CNN200', 'CUST200', 'CUSTNN', 'CUSTNNLOAD', 'REINFORCE200', 'LSTM','BERT']:
            n_hidden = [200, 200] #2 hidden layers
        elif args.policy == 'MLP400':
            n_hidden = [400, 250, 100] #3 hidden layers
        elif args.policy == 'MLP1':
            n_hidden = [300, 200, 1] #3 hidden layers
        elif args.policy == 'MLP64':
            n_hidden = [64, 64] #3 hidden layers
    elif args.policy in ['MLPLSTM200', 'CNNLSTM200', 'MLPLSTMLN200', 'MLP4CPU', 'CUSTNNCPU']:
        if args.observe == 'FLAGRUN':
            import pybulletgym
            env = DummyVecEnv([lambda: Monitor(gym.make('HumanoidFlagrunPyBulletEnv-v0'),SAVE_PATH + "_l", allow_early_resets=True),]*args.cpus)
        else:
            env = DummyVecEnv([lambda: BioASQEnv(),]*args.cpus)
        n_hidden = [200, 200] #2 hidden layers
        #if args.thread_test:
        #    env_test = DummyVecEnv([lambda: BioASQEnv(),]*cpus)
        #    env_test.envs[0].qid = 0

    if args.model == 'FIRSTN':
        #test = np.mean(get_reward_firstn(alldata))
        test = np.mean(get_reward_firstn(test_indices))
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#%s\n" % (test))
        #test = np.mean(get_reward_firstn(test_indices))
        print("FirstN Average (AllData): %s" % test)
    elif args.model == 'LASTN':
        #test = np.mean(get_reward_firstn(alldata, last=True))
        test = np.mean(get_reward_firstn(test_indices, last=True))
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#%s\n" % (test))
        #test = np.mean(get_reward_firstn(test_indices))
        print("LastN Average (AllData): %s" % test)
    elif args.model == 'RANDOM':
        #test = np.mean(get_reward_firstn(alldata,_random=True))
        test = np.mean(get_reward_firstn(test_indices,_random=True))
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#%s\n" % (test))
        print("Random Average (AllData): %s" % test)
    elif args.model == 'ALL':
        #test = np.mean(get_reward_firstn(alldata,all=True))
        test = np.mean(get_reward_firstn(test_indices,all=True))
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#%s\n" % (test))
        print("Select All Average (AllData): %s" % test)
    elif args.load in ['RETRAIN', 'TEST', 'BioASQ']:
        if args.observe == 'BERT' and args.policy != 'BERT':
            try:
                meanEmbeddings.model.load("BERT_EMBEDDINGS")
            except:
                pass
        if args.model in ['REINFORCE', 'CLASSIFICATION']:
            print("Loaded Model REINFORCE (for RETRAIN)")
            policy = NNModel(env.envs[0].observation_space.shape,n_hidden)
            #policy.model_classify = keras.models.load_model(SAVE_PATH + ".ckpt")
            #policy.model_predict = keras.models.load_model(SAVE_PATH + ".ckpt")
            policy.model_classify.load_weights(SAVE_PATH + ".ckpt")
            policy.model_predict.load_weights(SAVE_PATH + ".ckpt")
            policy.model_train.load_weights(SAVE_PATH + ".ckpt")
            #policy.model_predict.load_weights(SAVE_PATH + ".ckpt")
            
            if args.load == 'TEST':
                test = np.mean(env.envs[0].test_reward(policy,test_indices,deterministic=True))
                print("TestData Average: %s" % test)
            elif args.load == 'BioASQ':
                out = env.envs[0].answers(policy,bioasqdata,deterministic=True)
                with open("%s_out.json" % args.model, 'w') as f:
                    f.write(json.dumps({"questions": out}, indent=2))
            elif args.load == 'RETRAIN':
                test = np.mean(env.envs[0].test_reward(policy,test_indices,deterministic=True))
                with open("%s_test.txt" % SAVE_PATH, "a") as f:
                    f.write("#%s\n" % (test))
                STEPS = 500
                reinforce_diego(env, args.timesteps,callback,policy)
            sys.exit()
        elif args.model == 'PPO1':
            model = PPO1.load(SAVE_PATH, env=env)
            STEPS = 1
        elif args.model == 'PPO2':
            model = PPO2.load(SAVE_PATH, env=env)
            STEPS = 1
        elif args.model == 'ACER':
            model = ACER.load(SAVE_PATH, env=env)
            STEPS = 1
        elif args.model == 'PPO5':
            model = PPO5.load(SAVE_PATH, env=env)
        elif args.model == 'PPO4':
            model = PPO4.load(SAVE_PATH, env=env)
        elif args.model == 'TRPO':
            model = TRPO.load(SAVE_PATH, env=env)
        elif args.model == 'A2C':
            model = A2C.load(SAVE_PATH, env=env)
        elif args.model == 'ACER2':
            model = ACER2.load(SAVE_PATH, env=env)
        elif args.model == 'DQN':
            STEPS = 1
            model = DQN.load(SAVE_PATH, env=env)
        elif args.model == 'DDPG':
            model = DDPG.load(SAVE_PATH, env=env)
        elif args.model == 'HER':
            model = HER.load(SAVE_PATH, env=env)
        elif args.model == 'SAC':
            model = SAC.load(SAVE_PATH, env=env)

        print("Loaded Model")
        if args.load == 'TEST':
            test = np.mean(env.envs[0].test_reward(model,test_indices,deterministic=False))
            print("TestData Average: %s" % test)
        elif args.load == 'BioASQ':
            out = env.envs[0].answers(model,bioasqdata,deterministic=False)
            with open("%s_out.json" % args.model, 'w') as f:
                f.write(json.dumps({"questions": out}, indent=2))
        elif args.load == 'RETRAIN':
            test = np.mean(env.envs[0].test_reward(model,test_indices,deterministic=False))
            with open("%s_test.txt" % SAVE_PATH, "a") as f:
                f.write("#%s\n" % (test))
            model.learn(total_timesteps=args.timesteps, callback=callback)
    elif args.model == 'CLASSIFICATION':
        policy = NNModel(env.envs[0].observation_space.shape,n_hidden)
        value = NNModel(env.envs[0].observation_space.shape,n_hidden)
        magic_tensors = []
        labels = []
        rewardlabels = []
        testX = []
        testY = []
        testR = []
        _labels = rouge_to_labels('rouge_7b.csv', labels="topn", threshold=args.topn, metric=['SU4'])
        for q in train_indices:
            done = False
            obs = env.envs[0].reset(q)
            count = 0
            while done == False:
                count += 1
                _label = _labels[(q,env.envs[0].id,env.envs[0].index)]
                labels.append(0 if _label else 1)
                magic_tensors.append(obs)
                obs, rewards, done, info = env.envs[0].step(0 if _label else 1)
                if done:
                    for i in range(count):
                        rewardlabels.append(rewards)
        
        for q in test_indices:
            done = False
            obs = env.envs[0].reset(q)
            count = 0
            while done == False:
                count += 1
                _label = _labels[(q,env.envs[0].id,env.envs[0].index)]
                testY.append(0 if _label else 1)
                testX.append(obs)
                obs, rewards, done, info = env.envs[0].step(0 if _label else 1)
                if done:
                    for i in range(count):
                        testR.append(rewards)
                        
        
        #print("Train/Test Sents: %s:%s" % (len(labels),len(testY)))
        #print("Total Sents: %s" % (len(labels) + len(testY)))

        #Weights to stop over-predicting "1", eg for [0,0,1,1,1,1] - weights = {0: 1, 1: 2/4} = even weighting of 1*2 0s and (1/2)*4 1s
        #args.ratio = amout to over-predict "0", as changing this ratio initially improved results (default = 1)
        #class_weight = {0: 1*args.weight, 1: labels.count(0) / labels.count(1)} 
        #print("Counts: %s:%s" % (labels.count(0),labels.count(1)))
        #print("Weights: %s:%s" % (class_weight[0],class_weight[1]))
        #weights = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
        class_weight = {0: 1*args.weight, 1: 1} 
        print("Weights: %s:%s" % (class_weight[0],class_weight[1]))# Calculate the weights for each class so that we can balance the data

        if args.observe == 'TFIDF': #remove extra dimension forced by TFIDF
            magic_tensors = np.reshape(np.array(magic_tensors), (len(magic_tensors),TFIDF_SIZE*5+1))
            testX = np.reshape(np.array(testX), (len(testX),TFIDF_SIZE*5+1))

        policy.model_classify.fit({"X": np.array(magic_tensors)}, np.array(labels), validation_data=({'X': np.array(testX)},  np.array(testY)), epochs=args.epochs, callbacks=[], batch_size=4096, class_weight=class_weight)
        policy.model_classify.save(SAVE_PATH + ".ckpt")

        try:
            value.model_regression.fit({"X": np.array(magic_tensors)}, np.array(rewardlabels), validation_data=({'X': np.array(testX)},  np.array(testR)), epochs=args.epochs, callbacks=[], batch_size=4096) # , class_weight=class_weight
            value.model_regression.save(SAVE_PATH + "_value.ckpt")
        except:
            print('Value Function ERROR!')
        #test
        test = np.mean(env.envs[0].test_reward(policy,test_indices))
        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#%s\n" % (test))
        print("TestData Average: %s" % test)
    else:
        if args.act == 'RELU':
            policy_kwargs = dict(act_fun=tf.nn.relu, layers=n_hidden)
        elif args.act == 'TANH':
            policy_kwargs = dict(act_fun=tf.nn.tanh, layers=n_hidden)
        elif args.act == 'SIGMOID':
            policy_kwargs = dict(act_fun=tf.nn.sigmoid, layers=n_hidden)
        elif args.act == 'NONE':
            policy_kwargs = dict(act_fun=keras.activations.linear, layers=n_hidden)
        if args.policy in ['MLP64','MLP200','MLP400', 'MLP4CPU', 'MLP1']:
            policy = MlpPolicy
        elif args.policy == 'MLPLSTM200':
            policy = MlpLstmPolicy
        elif args.policy == 'CNN200':
            policy = CnnPolicy #Only works for images - can't use!
        elif args.policy == 'MLPLSTMLN200':
            policy = MlpLnLstmPolicy
        elif args.policy == 'CUST200':
            policy = CustomPolicy
        elif args.policy in ['CUSTNN', 'CUSTNNLOAD', 'CUSTNNCPU']:
            policy = CustomNN
        elif args.policy in ['BERT']:
            policy = CustomEMBEDDINGS
        elif args.policy in ['LSTM']:
            policy = CustomLSTM
        elif args.policy == 'REINFORCE200':
            policy = NNModel(env.envs[0].observation_space.shape,n_hidden)
        
        if args.model == 'PPO1':
            STEPS = 1 #args.batch_size
            model = PPO1(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, timesteps_per_actorbatch=args.batch_size)
        elif args.model == 'PPO2':
            if args.batch_size == 100:
                STEPS = 10
            else:
                STEPS = 1
            model = PPO2(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, n_steps=args.batch_size, nminibatches=args.minibatches) #,learning_rate=0.001)
        elif args.model == 'PPO3':
            if args.batch_size == 100:
                STEPS = 10
            else:
                STEPS = 1
            model = PPO2(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, n_steps=args.batch_size, nminibatches=args.minibatches) #,learning_rate=0.001)
        elif args.model == 'PPO5':
            STEPS = 100
            model = PPO5(NNModel(env.envs[0].observation_space.shape,n_hidden), env, gamma=GAMMA, verbose=1, n_steps=25)
        elif args.model == 'PPO4':
            STEPS = 100
            model = PPO4(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, n_steps=25)
        elif args.model == 'TRPO':
            STEPS = 50
            model = TRPO(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, timesteps_per_batch=args.batch_size) #prev 1024
        elif args.model == 'A2C':
            STEPS = 500
            model = A2C(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1)
        elif args.model == 'ACER':
            #STEPS = 1
            #model = ACER(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, n_steps=args.batch_size) #n_steps=20
            STEPS = 50
            model = ACER(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, n_steps=args.batch_size)
            #args.batch_size = 20
        elif args.model == 'ACER2':
            STEPS = 500
            model = ACER2(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1)
        elif args.model == 'DDPG':
            #add noise for exploration.../ (example!)
            param_noise = AdaptiveParamNoiseSpec(initial_stddev=0.1, desired_action_stddev=0.1)
            STEPS = 500
            model = DDPG(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, param_noise=param_noise, verbose=1)
        elif args.model == 'HER':
            STEPS = 500
            #action_noise = NormalActionNoise(mean=np.zeros(2), sigma=0.2 * np.ones(2))
            model = HER(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1) #, action_noise=action_noise
        elif args.model == 'SAC':
            policy_kwargs = dict(act_fun=tf.nn.sigmoid, layers=n_hidden, n_env=len(env.envs), n_steps=args.batch_size, n_batch=4)
            STEPS = 500
            #action_noise = NormalActionNoise(mean=np.zeros(2), sigma=0.2 * np.ones(2))
            model = SAC(policy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1)
        elif args.model == 'DQN':
            #policy_kwargs = dict(act_fun=tf.nn.sigmoid, layers=n_hidden)
            #STEPS = 500
            STEPS = 500
            #if 'CNN' in args.policy:
            #    model = DQN(DCnnPolicy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1, batch_size=args.batch_size)
            #else:
            model = DQN(DMlpPolicy, env, gamma=GAMMA, policy_kwargs=policy_kwargs, verbose=1) #, batch_size=args.batch_size
        elif args.model == 'REINFORCE':
            STEPS = 500
            reinforce_diego(env, args.timesteps,callback,policy)
            sys.exit()
        print("Created Model")
        
        #learn:
        model.learn(total_timesteps=args.timesteps, callback=callback)

        with open("%s_test.txt" % SAVE_PATH, "a") as f:
            f.write("#TIME: %s\n" % str(datetime.datetime.now()))

#env = DummyVecEnv([lambda: BioASQEnv(),lambda: BioASQEnv(),lambda: BioASQEnv(),lambda: BioASQEnv(),])
#model = PPO2(MlpLstmPolicy, env, verbose=1)
