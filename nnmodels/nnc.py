"NN models for classification"
import tensorflow as tf
from tensorflow import keras
import numpy as np
import sqlite3
import random
import os
import glob
import sys
from multiprocessing import Pool
from functools import partial

from sklearn.metrics import mean_squared_error

from nnmodels import compare

EMBEDDINGS = 100
VECTORS = 'allMeSH_2016_%i.vectors.txt' % EMBEDDINGS
DB = 'word2vec_%i.db' % EMBEDDINGS
MAX_NUM_SNIPPETS = 50
SENTENCE_LENGTH = 300

print("LSTM using sentence length=%i" % SENTENCE_LENGTH)
print("LSTM using embedding dimension=%i" % EMBEDDINGS)

with open(VECTORS) as v:
    VOCABULARY = int(v.readline().strip().split()[0]) + 2

if not os.path.exists(DB):
    print("Creating database of vectors %s" % DB)
    conn = sqlite3.connect(DB)
    c = conn.cursor()
    c.execute("""CREATE TABLE vectors (word unicode,
                                       word_index integer,
                                       data unicode)""")
    with open(VECTORS, encoding='utf-8') as v:
        nwords = int(v.readline().strip().split()[0])
        print("Processing %i words" % nwords)
        zeroes = " ".join("0"*EMBEDDINGS)
        # Insert PAD and UNK special words with zeroes
        c.execute("INSERT INTO vectors VALUES (?, ?, ?)", ('PAD', 0, zeroes))
        c.execute("INSERT INTO vectors VALUES (?, ?, ?)", ('UNK', 1, zeroes))
        for i in range(nwords):
            vector = v.readline()
            windex = vector.index(" ")
            w = vector[:windex].strip()
            d = vector[windex:].strip()
            assert len(d.split()) == EMBEDDINGS
            #if i < 5:
            #    print(w)
            #    print(d)
            c.execute("INSERT INTO vectors VALUES (?, ?, ?)", (w, i+2, d))
    c.execute("CREATE INDEX word_idx ON vectors (word)")
    conn.commit()
    conn.close()

#vectordb = sqlite3.connect(DB)

def sentences_to_ids(sentences, sentence_length=SENTENCE_LENGTH):
    """Convert each sentence to a list of word IDs.

Crop or pad to 0 the sentences to ensure equal length if necessary.
Words without ID are assigned ID 1.
>>> sentences_to_ids([['my','first','sentence'],['my','ssecond','sentence'],['yes']], 2)
(([11095, 121], [11095, 1], [21402, 0]), (2, 2, 1))
"""
    return tuple(zip(*map(partial(one_sentence_to_ids,
                                  sentence_length=sentence_length),
                          sentences)))

def one_sentence_to_ids(sentence, sentence_length=SENTENCE_LENGTH):
    """Convert one sentence to a list of word IDs."

Crop or pad to 0 the sentences to ensure equal length if necessary.
Words without ID are assigned ID 1.
>>> one_sentence_to_ids(['my','first','sentence'], 2)
([11095, 121], 2)
>>> one_sentence_to_ids(['my','ssecond','sentence'], 2)
([11095, 1], 2)
>>> one_sentence_to_ids(['yes'], 2)
([21402, 0], 1)
"""
    vectordb = sqlite3.connect(DB)
    c = vectordb.cursor()
    word_ids = []
    for w in sentence:
        if len(word_ids) >= sentence_length:
            break
        c.execute("""SELECT word_index, word
                  FROM vectors
                  INDEXED BY word_idx
                  WHERE word=?""", (w, ))
        r = c.fetchall()
        if len(r) > 0:
            word_ids.append(r[0][0])
        else:
            word_ids.append(1)
    # Pad with zeros if necessary
    num_words = len(word_ids)
    if num_words < sentence_length:
        word_ids += [0]*(sentence_length-num_words)
    vectordb.close()
    return word_ids, num_words

def parallel_sentences_to_ids(sentences, sentence_length=SENTENCE_LENGTH):
    """Convert each sentence to a list of word IDs.

Crop or pad to 0 the sentences to ensure equal length if necessary.
Words without ID are assigned ID 1.
>>> parallel_sentences_to_ids([['my','first','sentence'],['my','ssecond','sentence'],['yes']], 2)
(([11095, 121], [11095, 1], [21402, 0]), (2, 2, 1))
"""
    with Pool() as pool:
        return tuple(zip(*pool.map(partial(one_sentence_to_ids,
                                           sentence_length=sentence_length),
                                   sentences)))

def snippets_to_ids(snippets, sentence_length, max_num_snippets=MAX_NUM_SNIPPETS):
    """Convert the snippets to lists of word IDs.
    >>> snippets_to_ids([['sentence', 'one'], ['sentence'], ['two']], 3, 2)
    (([12205, 68, 0], [12205, 0, 0]), (2, 1))
    >>> snippets_to_ids([['sentence', 'three']], 3, 2)
    (([12205, 98, 0], [0, 0, 0]), (2, 0))
    """
    # Pad to the maximum number of snippets
    working_sample = snippets[:max_num_snippets]
    #print("Number of snips: %i" % len(sample))
    if len(working_sample) < max_num_snippets:
        working_sample += [[]] * (max_num_snippets-len(working_sample))

    # Convert to word IDs
    return sentences_to_ids(working_sample, sentence_length)

def parallel_snippets_to_ids(batch_snippets,
                             sentence_length,
                             max_num_snippets=MAX_NUM_SNIPPETS):
    """Convert the batch of snippets to lists of word IDs.
    >>> parallel_snippets_to_ids([[['sentence', 'one'], ['sentence'], ['two']],[['sentence', 'three']]], 3, 2)
    ((([12205, 68, 0], [12205, 0, 0]), ([12205, 98, 0], [0, 0, 0])), ((2, 1), (2, 0)))
    """
    with Pool() as pool:
        return tuple(zip(*pool.map(partial(snippets_to_ids,
                                           sentence_length=sentence_length,
                                           max_num_snippets=max_num_snippets),
                                   batch_snippets)))

def embeddings_one_sentence(row):
    return [float(x) for x in row[1].split()]

class BasicNN:
    """A simple NN classifier"""
    def __init__(self, sentence_length=SENTENCE_LENGTH, batch_size=128, embeddings=True,
                 hidden_layer=0, build_model=False):
        self.batch_size = batch_size
        self.sentence_length = sentence_length
        self.embeddings = None
        self.hidden_layer = hidden_layer
        self.build_model = build_model
        if embeddings:
            vectordb = sqlite3.connect(DB)
            print("Database %s opened" % DB)
            c = vectordb.cursor()
            c_iterator = c.execute("""SELECT word_index, data
                                      FROM vectors""")
            print("Loading word embeddings")
            with Pool() as pool:
                self.embeddings = pool.map(embeddings_one_sentence,
                                           c_iterator)
            print("Word embeddings loaded")
            vectordb.close()

    def name(self):
        if self.embeddings == None:
            str_embeddings = ""
        else:
            str_embeddings = "(embed%i)" % EMBEDDINGS
        if self.hidden_layer == 0:
            str_hidden = ""
        else:
            str_hidden = "-relu(%i)" % self.hidden_layer
        return "BasicNN%s%s" % (str_embeddings, str_hidden)


    def __build_model__(self, learningrate=0.001, keep_prob=0.5, verbose=1):
        model = keras.Sequential()
        model.add(keras.layers.Embedding(VOCABULARY, EMBEDDINGS))
        model.add(keras.layers.GlobalAveragePooling1D())
        if self.hidden_layer > 0:
            model.add(keras.layers.Dense(self.hidden_layer, activation=tf.nn.relu))
        model.add(keras.layers.Dropout(1 - keep_prob)) # Is this correct??
        model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

        if self.embeddings != None:
            model.layers[0].set_weights([np.array(self.embeddings)])
            model.layers[0].trainable = False

        model.compile(optimizer=tf.train.AdamOptimizer(learningrate),
                      loss='binary_crossentropy',
                      metrics=['accuracy'])

        if verbose:
            model.summary()

        return model

    def restore(self, savepath):
        model = self.__build_model__()
        model.load_weights(savepath)
        self.model=model
        print("Model restored from file: %s" % savepath)

    def fit(self, X_train, Y_train,
            verbose=2, nb_epoch=3,
            learningrate=0.001, dropoutrate=0.5, savepath=None,
            restore_model=False):

        if restore_model:
            print("Restoring BasicNN model from %s" % savepath)
            self.model.load_weights(savepath)
            return self.test(X_train, None, Y_train)

        # Training loop
        print("Extracting sentence IDs")
        X, sequence_lengths = parallel_sentences_to_ids(X_train, self.sentence_length)
        print("Sentence IDs extracted")
        return self.__fit__(X, None, None, Y_train,
                            verbose, nb_epoch, learningrate,
                            dropoutrate, savepath)

    def __fit__(self, X, Y,
                verbose, nb_epoch, learningrate, keep_prob,
                savepath=None):
        self.model = self.__build_model__(learningrate, keep_prob, verbose)

        X = np.array(X)
        sequence_lengths = np.array(sequence_lengths)

        if savepath:
            callbacks = [tf.keras.callbacks.ModelCheckpoint(savepath,
                                                            save_weights_only=True,
                                                            verbose=1)]
        else:
            callbacks = []

        history = self.model.fit(X, Y, epochs=nb_epoch, callbacks=callbacks, batch_size=self.batch_size)

        return history.history['loss']

    def predict(self, X_topredict):
        X, sequence_lengths = parallel_sentences_to_ids(X_topredict, self.sentence_length)
        return self.model.predict(X)

    def test(self, X_test, Y_test):
        X, sequence_lengths = parallel_sentences_to_ids(X_test, self.sentence_length)
        return self.__test__(X, Y_test)

    def __test__(self, X, Y):
        X = np.array(X)
        #return self.model.fit(X, Y, epochs=3, batch_size=self.batch_size)
        return self.model.evaluate(X, Y, batch_size=self.batch_size)

class Similarities(BasicNN):
    """A classifier that incorporates similarity operations"""
    def __init__(self, sentence_length=SENTENCE_LENGTH, batch_size=128, embeddings=True,
                 hidden_layer=0, build_model=False, comparison=compare.SimMul(), positions=False, regression=False, reinforce=False):
        BasicNN.__init__(self, sentence_length, batch_size, embeddings,
                 hidden_layer, build_model)
        self.comparison = comparison
        self.positions = positions
        self.regression_model = regression
        self.reinforce = reinforce
        self.meanEmbeddings = None

    def name(self):
        if self.positions:
            str_positions = "+pos"
        else:
            str_positions = ""
        if self.embeddings == None:
            str_embeddings = ""
        else:
            str_embeddings = "(embed%i)" % EMBEDDINGS
        if self.hidden_layer == 0:
            str_hidden = ""
        else:
            str_hidden = "-relu(%i)" % self.hidden_layer
        return "Mean%s%s%s%s" % (self.comparison.name, str_embeddings, str_positions, str_hidden)

    def embedding_reduction(self, word_embeddings, keep_prob):
        "Return the sentence embeddings based on word embeddings"
        return keras.layers.GlobalAveragePooling1D()(word_embeddings)

    def __build_model__(self,
            #embeddings_lambda=10,
                        learningrate=0.001, keep_prob=0.5, verbose=1):

        if self.reinforce:
            class MeanOfEmbeddings():
                def __init__(self,_sentence_length,_embeddings):
                    ###X1 Sentence (All Text)
                    X1_input = keras.layers.Input(shape=(_sentence_length,), name='X')
                    if _embeddings == None:
                        embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS)(X1_input)
                    else:
                        embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                                 weights=[np.array(_embeddings)],
                                                                 trainable=False)(X1_input)
                    X1_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s1)
                    self.model = keras.models.Model([X1_input], X1_meanembeddings)
            print("EMBEDDINGS: %s" % self.embeddings)
            self.meanEmbeddings = MeanOfEmbeddings(self.sentence_length, self.embeddings)

            X_input = keras.layers.Input(shape=(EMBEDDINGS*5+1,), name='X') #TODO: make 100*5+1
            X_dropout = keras.layers.Dropout(1 - keep_prob)(X_input)
            Q_input = None
            #hidden = keras.layers.Dense(n_hidden, activation="relu")(X_state) 
            sim = X_dropout
            positions = None
            
            # Concatenate all inputs
            all_inputs = X_dropout
        else:
            # Sentence
            X_input = keras.layers.Input(shape=(self.sentence_length,), name='X')
            if self.embeddings == None:
                embedding_s = keras.layers.Embedding(VOCABULARY, EMBEDDINGS)(X_input)
            else:
                embedding_s = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                     weights=[np.array(self.embeddings)],
                                                     trainable=False)(X_input)
            embedding_s_reduction = self.embedding_reduction(embedding_s, keep_prob)
            X_dropout = keras.layers.Dropout(1 - keep_prob)(embedding_s_reduction)

            # Question
            Q_input = keras.layers.Input(shape=(self.sentence_length,), name='Q')
            if self.embeddings == None:
                embedding_q = keras.layers.Embedding(VOCABULARY, EMBEDDINGS)(Q_input)
            else:
                embedding_q = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                     weights=[np.array(self.embeddings)],
                                                     trainable=False)(Q_input)
            embedding_q_reduction = self.embedding_reduction(embedding_q, keep_prob)
            Q_output = keras.layers.Dropout(1 - keep_prob)(embedding_q_reduction)

            # Similarity
            sim = keras.layers.Multiply()([X_dropout, Q_output])

            # Sentence position
            if self.positions:
                positions = [keras.layers.Input(shape=(1,), name='X2')]
            else:
                positions = []

            # Concatenate all inputs
            all_inputs = keras.layers.Concatenate()([X_dropout, sim] + positions)

        # Hidden layer
        if self.hidden_layer > 0:
            hidden = keras.layers.Dense(self.hidden_layer, activation=tf.nn.relu)(all_inputs)
        else:
            hidden = all_inputs
        dropout_2 = keras.layers.Dropout(1 - keep_prob)(hidden)

        return self.top_layer(dropout_2, X_input, Q_input, positions, learningrate, verbose)

    def top_layer(self, dropout_2, X_input, Q_input, positions, learningrate, verbose):
        # Final prediction
        if self.reinforce:
            inputs = [X_input,]
        else:
            inputs = [X_input, Q_input] + positions
        if not(self.regression_model):
            print("top_layer: classification model (sigmoid activation)")
            output = keras.layers.Dense(1, activation=tf.nn.sigmoid)(dropout_2)
            model = keras.models.Model(inputs, output)
            model.compile(optimizer=tf.train.AdamOptimizer(learningrate),
                          loss='binary_crossentropy',
                          metrics=['accuracy'])
        else:
            print("top_layer: regression model (linear activation)")
            output = keras.layers.Dense(1, activation=None)(dropout_2)
            model = keras.models.Model(inputs, output)
            model.compile(optimizer=tf.train.AdamOptimizer(learningrate),
                                  loss='mean_squared_error',
                                  metrics=['accuracy'])

        if verbose:
            model.summary()

        return model

    def fit(self, X_train, Q_train, Y_train,
            X_positions = [],
            validation_data = None,
            verbose=2, nb_epoch=3,
            learningrate=0.001, dropoutrate=0.5, savepath=None,
            restore_model=False):

        if restore_model:
            print("Restoring Similarities model from %s" % savepath)
            #self.model.load_weights(savepath)
            self.restore(savepath)
            return self.test(X_train, Q_train, Y_train, X_positions=X_positions)

        assert(len(X_train) == len(Q_train))
        assert(len(X_train) == len(Y_train))

        # Training loop
        X, sequence_lengths_x = parallel_sentences_to_ids(X_train, self.sentence_length)
        Q, sequence_lengths_q = parallel_sentences_to_ids(Q_train, self.sentence_length)

        if validation_data:
            X_val, Q_val, Y_val, Xpos_val = validation_data
            X_val, sequence_lengths_x_val = parallel_sentences_to_ids(X_val, self.sentence_length)
            Q_val, sequence_lengths_q_val = parallel_sentences_to_ids(Q_val, self.sentence_length)
            val_data = ({'X': np.array(X_val),
                         'Q': np.array(Q_val),
                         'X2': np.array(Xpos_val)},
                        np.array(Y_val))
        else:
            val_data = None

        return self.__fit__(X, X_positions, Q, Y_train,
                            val_data,
                            verbose, nb_epoch, learningrate,
                            dropoutrate, savepath)

    def __fit__(self, X, X_positions, Q, Y,
                val_data,
                verbose, nb_epoch, learningrate, keep_prob,
                savepath=None):
        self.model = self.__build_model__(learningrate, keep_prob, verbose)

        X = np.array(X)
        Q = np.array(Q)

        #savepath = None

        if verbose == 1 or verbose == 2:
            verbose = 3 -  verbose
        if self.reinforce:
            X_1 = self.meanEmbeddings.model.predict(X)
            Q_1 = self.meanEmbeddings.model.predict(Q)
            array = np.hstack([ X_1, X_1, X_1, X_1, X_1, X_positions ]) #Q_1
            #array2 = np.hstack([ X_1, X_1, X_1, X_1, X_positions ]) #Q_1
        
            val_X = self.meanEmbeddings.model.predict(val_data[0]['X'])
            val_Q = self.meanEmbeddings.model.predict(val_data[0]['Q'])
            val_X2 = val_data[0]['X2']
            val_data2 = ({'X': np.hstack([ val_X, val_X, val_X, val_Q, val_Q, val_X2 ]), },  np.array(val_data[1]))
            print(val_data)
            print(array.shape)
            history = self.model.fit({'X':array}, Y,
                                     validation_data=val_data2,
                                     verbose=verbose,
                                     epochs=nb_epoch, callbacks=[], batch_size=self.batch_size)
        else:
            history = self.model.fit({'X':X, 'Q': Q, 'X2': X_positions}, Y,
                                     validation_data=val_data,
                                     verbose=verbose,
                                     epochs=nb_epoch, callbacks=[], batch_size=self.batch_size)

        if savepath:
            print ("Save: %s" % savepath)
            self.model.save_weights(savepath)
        return history.history

    def predict(self, X_topredict, Q_topredict, X_positions=[]):
        assert(len(X_topredict) == len(Q_topredict))
        if len(X_topredict) == 0:
            print("WARNING: Data to predict is an empty list")
            return []
        X, sequence_lengths_x = parallel_sentences_to_ids(X_topredict, self.sentence_length)
        Q, sequence_lengths_q = parallel_sentences_to_ids(Q_topredict, self.sentence_length)
        X = np.array(X)
        Q = np.array(Q)
        if self.reinforce:
            X_1 = self.meanEmbeddings.model.predict(X)
            Q_1 = self.meanEmbeddings.model.predict(Q)
            return self.model.predict({'X':np.hstack([ X_1, X_1, X_1, X_1, X_1, np.array(X_positions) ])  })
        else:
            return self.model.predict({'X':X, 'Q': Q, 'X2': np.array(X_positions)})

    def test(self, X_test, Q_test, Y, X_positions=[]):
        assert(len(X_test) == len(Q_test))
        assert(len(X_test) == len(Y))

        X, sequence_lengths_x = parallel_sentences_to_ids(X_test, self.sentence_length)
        Q, sequence_lengths_q = parallel_sentences_to_ids(Q_test, self.sentence_length)
        return self.__test__(X,
                             Q,
                             Y,
                             X_positions)

    def __test__(self, X,
                       Q,
                       Y,
                       X_positions):
        X = np.array(X)
        Q = np.array(Q)
        return self.model.evaluate({'X':X, 'Q': Q, 'X2': X_positions}, Y, batch_size=self.batch_size)

class CNNSimilarities(Similarities):
    """A classifier that incorporates similarity operations"""
    NGRAMS = (2, 3, 4)
    CONVOLUTION = 32

    def name(self):
        if self.embeddings == None:
            str_embeddings = ""
        else:
            str_embeddings = "(embed%i)" % EMBEDDINGS
        if self.hidden_layer == 0:
            str_hidden = ""
        else:
            str_hidden = "-relu(%i)" % self.hidden_layer
        return "CNN%s%s%s" % (self.comparison.name, str_embeddings, str_hidden)

    def embedding_reduction(self, word_embeddings, keep_prob):
        convs = []
        for ngram in self.NGRAMS:
            c = keras.layers.Conv1D(self.CONVOLUTION, ngram, activation='relu')(word_embeddings)
            m = keras.layers.GlobalMaxPooling1D()(c)
            convs.append(m)
        return keras.layers.Concatenate()(convs)

class LSTMSimilarities(Similarities):
    """A classifier that incorporates similarity operations"""
    def name(self):
        if self.positions:
            str_positions = "+pos"
        else:
            str_positions = ""
        if self.embeddings == None:
            str_embeddings = ""
        else:
            str_embeddings = "(embed%i)" % EMBEDDINGS
        if self.hidden_layer == 0:
            str_hidden = ""
        else:
            str_hidden = "-relu(%i)" % self.hidden_layer
        return "iLSTM%s%s%s%s" % (self.comparison.name, str_embeddings, str_positions, str_hidden)

    def embedding_reduction(self, word_embeddings, keep_prob):
        return keras.layers.Bidirectional(keras.layers.LSTM(EMBEDDINGS, dropout=1-keep_prob))(word_embeddings)

if __name__ == "__main__":
    import doctest
    import codecs
    doctest.testmod()

    # sys.exit()

    import csv
    import re

    def rouge_to_labels(rougeFile, labels, labelsthreshold, metric=["SU4"]):
        """Convert ROUGE values into classification labels
        >>> labels = rouge_to_labels("rouge_6b.csv", "topn", 3)
        Setting top 3 classification labels
        >>> labels[(0, '55031181e9bde69634000014', 9)]
        False
        >>> labels[(0, '55031181e9bde69634000014', 20)]
        True
        >>> labels[(0, '55031181e9bde69634000014', 3)]
        True
        """
        assert labels in ["topn", "threshold"]

        # Collect ROUGE values
        rouge = dict()
        with codecs.open(rougeFile,'r','utf-8') as f:
            reader = csv.reader(f)
            header = next(reader)
            index = [header.index(m) for m in metric]
            for line in reader:
                try:
                    key = (int(line[0]),line[1],int(line[2]))
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    print([l.encode('utf-8') for l in line])
                else:
                    rouge[key] = np.mean([float(line[i]) for i in index])

        # Convert ROUGE values into classification labels
        result = dict()
        if labels == "threshold":
            print("Setting classification labels with threshold", labelsthreshold)
            for key, value in rouge.items():
                result[key] = (value >= labelsthreshold)
            qids = set(k[0] for k in rouge)
            # Regardless of threshold, set top ROUGE of every question to True
            for qid in qids:
                qid_items = [(key, value) for key, value in rouge.items() if key[0] == qid]
                qid_items.sort(key = lambda k: k[1])
                result[qid_items[-1][0]] = True
        else:
            print("Setting top", labelsthreshold, "classification labels")
            qids = set(k[0] for k in rouge)
            for qid in qids:
                qid_items = [(key, value) for key, value in rouge.items() if key[0] == qid]
                qid_items.sort(key = lambda k: k[1])
                for k, v in qid_items[-labelsthreshold:]:
                    result[k] = True
                for k, v in qid_items[:-labelsthreshold]:
                    result[k] = False
        return result

    cleantext = lambda t: re.sub('[.,?;*!%^&_+():-\[\]{}]', '',
                                     t.replace('"', '').replace('/', '').replace('\\', '').replace("'",
                                                                                                   '').strip().lower()).split()

    labels_dict = rouge_to_labels('rouge_7b.csv', "topn", 5)
    sentences = []
    labels = []
    s_ids = []
    with open('rouge_7b.csv', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            sentences.append(cleantext(row['sentence text']))
            labels.append(labels_dict[(int(row['qid']), row['pubmedid'], int(row['sentid']))])
            s_ids.append([int(row['sentid'])])
    print("Data has %i items" % len(sentences))
    #print(sentences[:3])
    #print(labels[:3])

#    nnc = BasicNN(hidden_layer=50, build_model=True)# , positions=True)
    nnc = Similarities(hidden_layer=50, build_model=True, positions=True)
    print("Training %s" % nnc.name())
    loss = nnc.fit(sentences[100:], sentences[100:], np.array(labels[100:]),
                   X_positions=np.array(s_ids[100:]),
                   verbose=2,
                   validation_data=(sentences[:100], sentences[:100], labels[:100], s_ids[:100]),
                   nb_epoch=3)
    print("Training loss of each epoch: %s" % (str(loss['loss'])))
    print("Validation loss of each epoch: %s" % (str(loss['val_loss'])))
    testloss = nnc.test(sentences[:100], sentences[:100], np.array(labels[:100]),
                        X_positions=np.array(s_ids[:100]))
    print("Test loss: %s" % (testloss[0]))
