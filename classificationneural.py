"""classification.py -- perform classification-based summarisation using deep learning architectures

Author: Diego Molla <dmollaaliod@gmail.com>
Created: 16/1/2019
"""

import tensorflow as tf # required for running in NCI Raijin
import json
import codecs
import csv
import sys
import os
import shutil
import re
import random
import glob
from subprocess import Popen, PIPE
from multiprocessing import Pool
from functools import partial
import numpy as np

from sklearn.model_selection import KFold

from nltk import sent_tokenize
from xml_abstract_retriever import getAbstract
from nnmodels import nnc, compare
from summariser.basic import answersummaries
from classification import rouge_to_labels



class MeanOfEmbeddings():
    def __init__(self):
        ###X1 Sentence (All Text)
        X1_input = keras.layers.Input(shape=(SENTENCE_LENGTH,), name='X')
        embedding_s1 = keras.layers.Embedding(VOCABULARY, EMBEDDINGS,
                                                 weights=[np.array(embeddings_list)],
                                                 trainable=False)(X1_input)
        X1_meanembeddings = keras.layers.GlobalAveragePooling1D()(embedding_s1)

        self.model = keras.models.Model([X1_input], X1_meanembeddings)
        
class NNModel():
    def __init__(self, vocabulary_size, hidden_list):
        X_state = keras.layers.Input(shape=vocabulary_size)
        reward = keras.layers.Input(shape=(1,))
        hidden = X_state #keras.layers.Concatenate()([X_state, Q_state])
        self.episode = tf.placeholder(tf.float32)
        self.initial_state = None
        
        for n_hidden in hidden_list:
            hidden = keras.layers.Dense(n_hidden, activation="relu")(hidden)
        outputs = keras.layers.Dense(1, activation="sigmoid")(hidden)

        def custom_loss(y_true, y_predict):
            "From https://github.com/breeko/Simple-Reinforcement-Learning-with-Tensorflow/blob/master/Part%202%20-%20Policy-based%20Agents%20with%20Keras.ipynb "
            #log_lik = keras.backend.log(y_true * (y_true - y_predict) + (1 - y_true) * (y_true + y_predict))
            loss = - (y_true * keras.backend.log(y_predict + 0.0000001) + (1 - y_true) * keras.backend.log(1 - y_predict + 0.0000001)) # Using the standard cross-entropy formula
            return keras.backend.mean(loss * reward, keepdims=True)

        self.model_train = keras.models.Model([X_state, reward], outputs)
        self.model_train.compile(loss=custom_loss, optimizer=keras.optimizers.Adam())

        self.model_predict = keras.models.Model([X_state], outputs)
#meanEmbeddings = MeanOfEmbeddings()

def bioasq_train(small_data=False, verbose=2):
    """Train model for BioASQ"""
    print("Training for BioASQ")
    classifier = Classification('BioASQ-training7b.json',
                           'rouge_7b.csv',
                           rouge_labels=True,
                           regression=True,
                           nb_epoch=50,
                           verbose=verbose,
                           classification_type="LSTMSimilarities",
                           embeddings=True,
                           hidden_layer=50,
                           dropout=0.5,
                           batch_size=4096)
    indices = list(range(len(classifier.data)))
    #if small_data:
    #    print("Training bioasq with small data")
    #    indices = indices[:20]
    classifier.train(indices, savepath="./models/MODEL11")

def bioasq_run(nanswers={"summary": 6,
                         "factoid": 2,
                         "yesno": 2,
                         "list": 3},
#               test_data='BioASQ-trainingDataset6b.json',
#               test_data='BioASQ-training7b.json',
#               test_data='phaseB_5b_01.json',
                test_data='BioASQ-task7bPhaseB-testset1',
               output_filename='bioasq-out-nnr.json'):
    """Run model for BioASQ"""
    print("Running bioASQ")
    classifier = Classification('BioASQ-training7b.json',
                           'rouge_7b.csv',
                           nb_epoch=10,
                           rouge_labels=False,
                           regression=False,
                           verbose=2,
                           classification_type="LSTMSimilarities",
                           embeddings=True,
                           hidden_layer=50,
                           dropout=0.7,
                           batch_size=4096)
    indices = list(range(len(classifier.data)))
    classifier.train(indices, savepath="./models/MODEL5", restore_model=True)
    testset = loaddata(test_data)
    print("LOADED")
    answers = yield_bioasq_answers(classifier,
                                   testset,
                                   nanswers={"summary": 6,
                                             "factoid": 2,
                                             "yesno": 2,
                                             "list": 3})
    result = {"questions":[a for a in answers]}
    print("Saving results in file %s" % output_filename)
    with open(output_filename, 'w') as f:
        f.write(json.dumps(result, indent=2))

def loaddata(filename):
    """Load the JSON data
    >>> data = loaddata('BioASQ-trainingDataset6b.json')
    Loading BioASQ-trainingDataset6b.json
    >>> len(data)
    2251
    >>> sorted(data[0].keys())
    ['body', 'concepts', 'documents', 'id', 'ideal_answer', 'snippets', 'type']
    """
    print("Loading", filename)
    data = json.load(open(filename, encoding="utf-8"))
    return data['questions']

def yield_candidate_text(questiondata, snippets_only=True):
    """Yield all candidate text for a question
    >>> data = loaddata("BioASQ-trainingDataset6b.json")
    Loading BioASQ-trainingDataset6b.json
    >>> y = yield_candidate_text(data[0], snippets_only=True)
    >>> next(y)
    ('55031181e9bde69634000014', 0, 'Hirschsprung disease (HSCR) is a multifactorial, non-mendelian disorder in which rare high-penetrance coding sequence mutations in the receptor tyrosine kinase RET contribute to risk in combination with mutations at other genes')
    >>> next(y)
    ('55031181e9bde69634000014', 1, "In this study, we review the identification of genes and loci involved in the non-syndromic common form and syndromic Mendelian forms of Hirschsprung's disease.")
    """
    past_pubmed = set()
    sn_i = 0
    for sn in questiondata['snippets']:
        if snippets_only:
            for s in sent_tokenize(sn['text']):
                yield (questiondata['id'], sn_i, s)
                sn_i += 1
            continue

        pubmed_id = os.path.basename(sn['document'])
        if pubmed_id in past_pubmed:
            continue
        past_pubmed.add(pubmed_id)
        file_name = os.path.join("Task6bPubMed", pubmed_id+".xml")
        sent_i = 0
        for s in sent_tokenize(getAbstract(file_name, version="0")[0]):
            yield (pubmed_id, sent_i, s)
            sent_i += 1

def yield_bioasq_answers(classifier, testset, nanswers=3):
    """Yield answer of each record for BioASQ shared task"""
    for r in testset:
        test_question = r['body']
        test_id = r['id']
        test_candidates = [(sent, sentid)
                           for (pubmedid, sentid, sent)
                           in yield_candidate_text(r)]
#        test_snippet_sentences = [s for snippet in r['snippets']
#                                  for s in sent_tokenize(snippet['text'])]
        if len(test_candidates) == 0:
            print("Warning: no text to summarise")
            test_summary = ""
        else:
            if isinstance(nanswers,dict):
                n = nanswers[r['type']]
            else:
                n = nanswers
            test_summary = " ".join(classifier.answersummaries([(test_question,
                                                                test_candidates,
                                                                n)])[0])
            #print("Test summary:", test_summary)

        if r['type'] == "yesno":
            exactanswer = "yes"
        else:
            exactanswer = ""

        yield {"id": test_id,
               "ideal_answer": test_summary,
               "exact_answer": exactanswer}

def collect_one_item(this_index, indices, testindices, data, labels):
    "Collect one item for parallel processing"
    qi, d = this_index
    if qi in indices:
        partition = 'main'
    elif testindices != None and qi in testindices:
        partition = 'test'
    else:
        return None

    this_question = d['body']

    if 'snippets' not in d:
        return None
    data_snippet_sentences = [s for sn in d['snippets']
                              for s in sent_tokenize(sn['text'])]

    if len(data_snippet_sentences) == 0:
        return None

    candidates_questions = []
    candidates_sentences = []
    candidates_sentences_ids = []
    label_data = []
    for pubmed_id, sent_id, sent in yield_candidate_text(d):
        candidates_questions.append(this_question)
        candidates_sentences.append(sent)
        candidates_sentences_ids.append(sent_id)
        label_data.append(labels[(qi, pubmed_id, sent_id)])

    return partition, label_data, candidates_questions, candidates_sentences, candidates_sentences_ids

class BaseClassification:
    """A base classification to be inherited"""
    def __init__(self, corpusFile, rougeFile, metric=['SU4'],
                 rouge_labels=True, labels="topn", labelsthreshold=5):
        """Initialise the classification system."""
        print("Reading data from %s and %s" % (corpusFile, rougeFile))
        self.data = loaddata(corpusFile)
        if not rouge_labels:
            #convert rouge -> label
            self.labels = rouge_to_labels(rougeFile, labels, labelsthreshold, metric=metric)
        else:
            #leave as rouge scores
            self.labels = dict()
            with codecs.open(rougeFile, encoding='utf-8') as csv_file:
                reader = csv.DictReader(csv_file)
                lineno = 0
                num_errors = 0
                for line in reader:
                    lineno += 1
                    try:
                        key = (int(line['qid']), line['pubmedid'], int(line['sentid']))
                    except:
                        num_errors += 1
                        print("Unexpected error:", sys.exc_info()[0])
                        print("%i %s" % (lineno, str(line).encode('utf-8')))
                    else:
                        self.labels[key] = np.mean([float(line[m]) for m in metric])
                if num_errors > 0:
                    print("%i data items were ignored out of %i because of errors" % (num_errors, lineno))

    def extractfeatures(self, questions, candidates_sentences):
        """ Return the features"""
        assert len(questions) == len(candidates_sentences)

        # The following two lines are based on the text cleaning code of
        # BioASQ for generating word embeddings with word2vec
        cleantext = lambda t: re.sub(r'[.,?;*!%^&_+():-\[\]{}]', '',
                                     t.replace('"', '').replace('/', '').replace('\\', '').replace("'",
                                                                                                   '').strip().lower()).split()

        return ([cleantext(sentence) for sentence in candidates_sentences],
                [cleantext(question) for question in questions])

    def _collect_data_(self, indices, testindices=None):
        """Collect the data given the question indices"""
        print("Collecting data")
        with Pool() as pool:
            collected = pool.map(partial(collect_one_item,
                                         indices=indices,
                                         testindices=testindices,
                                         data=self.data,
                                         labels=self.labels),
                                 enumerate(self.data))
        all_candidates_questions = {'main':[], 'test':[]}
        all_candidates_sentences = {'main':[], 'test':[]}
        all_candidates_sentences_ids = {'main':[], 'test':[]}
        all_labels = {'main':[], 'test':[]}
        for c in collected:
            if c == None:
                continue
            partition, labels_data, candidates_questions, candidates_sentences, candidates_sentences_ids = c
            all_candidates_questions[partition] += candidates_questions
            all_candidates_sentences[partition] += candidates_sentences
            all_candidates_sentences_ids[partition] += candidates_sentences_ids
            all_labels[partition] += labels_data

        print("End collecting data")
        return all_labels, all_candidates_questions, all_candidates_sentences, all_candidates_sentences_ids

class Classification(BaseClassification):
    """A classification system"""
    def __init__(self, corpusFile, rougeFile, metric=['SU4'],
                 rouge_labels=True, labels="topn", labelsthreshold=5,
                 nb_epoch=3, verbose=2,
                 classification_type="Bi-LSTM",
                 embeddings=True,
                 hidden_layer=0,
                 dropout=0.5,
                 regression=False,
                 reinforce=False,
                 batch_size=128):
        """Initialise the classification system."""
        BaseClassification.__init__(self, corpusFile, rougeFile, metric=metric,
                                    rouge_labels=rouge_labels, labels=labels, labelsthreshold=labelsthreshold)
        self.nb_epoch = nb_epoch
        self.verbose = verbose
        self.dropout = dropout

        self.nnc = None
        if classification_type == "BasicNN":
            self.nnc = nnc.BasicNN(embeddings=embeddings,
                                    hidden_layer=hidden_layer,
                                    batch_size=batch_size)
        elif classification_type == "Similarities":
            self.nnc = nnc.Similarities(embeddings=embeddings,
                                          hidden_layer=hidden_layer,
                                          batch_size=batch_size,
                                          comparison=compare.SimMul(),
                                          regression=regression,
                                          reinforce=reinforce,
                                          positions=True)
        elif classification_type == "SimilaritiesYu":
            self.nnc = nnc.Similarities(embeddings=embeddings,
                                          hidden_layer=hidden_layer,
                                          batch_size=batch_size,
                                          comparison=compare.SimYu(),
                                          regression=regression,
                                          reinforce=reinforce,
                                          positions=True)
        elif classification_type == "SimilaritiesEuc":
            self.nnc = nnc.Similarities(embeddings=embeddings,
                                          hidden_layer=hidden_layer,
                                          batch_size=batch_size,
                                          comparison=compare.SimEuc(),
                                          regression=regression,
                                          reinforce=reinforce,
                                          positions=True)
        elif classification_type == "CNNSimilarities":
            self.nnc = nnc.CNNSimilarities(embeddings=embeddings,
                                             hidden_layer=hidden_layer,
                                             batch_size=batch_size,
                                             comparison = compare.SimMul())
        elif classification_type == "CNNSimilaritiesYu":
            self.nnc = nnc.CNNSimilarities(embeddings=embeddings,
                                             hidden_layer=hidden_layer,
                                             batch_size=batch_size,
                                             comparison = compare.SimYu())
        elif classification_type == "CNNSimilaritiesEuc":
            self.nnc = nnc.CNNSimilarities(embeddings=embeddings,
                                             hidden_layer=hidden_layer,
                                             batch_size=batch_size,
                                             comparison = compare.SimEuc())
        elif classification_type == "LSTMSimilarities":
            self.nnc = nnc.LSTMSimilarities(embeddings=embeddings,
                                              hidden_layer=hidden_layer,
                                              batch_size=batch_size,
                                              comparison = compare.SimMul(),
                                              regression=regression,
                                              reinforce=reinforce,
                                              positions=True)
        elif classification_type == "LSTMSimilaritiesYu":
            self.nnc = nnc.LSTMSimilarities(embeddings=embeddings,
                                              hidden_layer=hidden_layer,
                                              batch_size=batch_size,
                                              comparison = compare.SimMul(),
                                              regression=regression,
                                              reinforce=reinforce,
                                              positions=True)
        elif classification_type == "LSTMSimilaritiesEuc":
            self.nnc = nnc.LSTMSimilarities(embeddings=embeddings,
                                              hidden_layer=hidden_layer,
                                              batch_size=batch_size,
                                              comparison = compare.SimEuc(),
                                              regression=regression,
                                              reinforce=reinforce,
                                              positions=True)

    def train(self, indices, testindices=None, foldnumber=0, restore_model=False,
              savepath=None,
              save_test_predictions=None):
        """Train the classifier given the question indices"""
        print("Gathering training data")
        if savepath is None:
            savepath="savedmodels/%s_%i" % (self.nnc.name(), foldnumber)
        all_labels, candidates_questions, candidates_sentences, candidates_sentences_ids = \
        self._collect_data_(indices, testindices)

        features = self.extractfeatures(candidates_questions['main'],
                                        candidates_sentences['main'])

        print("Training %s" % self.nnc.name())
        if testindices == None:
            validation_data = None
        else:
            features_test = self.extractfeatures(candidates_questions['test'],
                                                 candidates_sentences['test'])
            validation_data = (features_test[0], features_test[1],
                               [[r] for r in all_labels['test']],
                               [[cid] for cid in candidates_sentences_ids['test']])
        loss_history = self.nnc.fit(features[0], features[1],
                                          np.array([[r] for r in all_labels['main']]),
                                          X_positions=np.array([[cid] for cid in candidates_sentences_ids['main']]),
                                          validation_data = validation_data,
                                          nb_epoch=self.nb_epoch,
                                          verbose=self.verbose,
                                          dropoutrate=self.dropout,
                                          savepath=savepath,
                                          restore_model=restore_model)
        if save_test_predictions:
            predictions_test = self.nnc.predict(features_test[0],
                                                features_test[1],
                                                X_positions=np.array([[cid] for cid in candidates_sentences_ids['test']]))
            predictions_test = [p[0] for p in predictions_test]
            print("Saving predictions in %s" % save_test_predictions)
            with open(save_test_predictions, "w") as f:
                writer = csv.DictWriter(f, fieldnames=["id", "target", "prediction"])
                writer.writeheader()
                for i, p in enumerate(predictions_test):
                    writer.writerow({"id": i,
                                     "target": all_labels['test'][i],
                                     "prediction": p})
                print("Predictions saved")
        if testindices:
            return loss_history['loss'][-1], loss_history['val_loss'][-1]
        else:
            return loss_history['loss'][-1]

    def test(self, indices):
        """Test the classifier given the question indices"""
        print("Gathering test data")
        all_labels, candidates_questions, candidates_sentences, candidates_sentences_ids = \
        self._collect_data_(indices)

        features = self.extractfeatures(candidates_questions['main'],
                                        candidates_sentences['main'])

        print("Testing NNC")
        loss = self.nnc.test(features[0], 
                              features[1],
                              [[r] for r in all_labels['main']],
                              X_positions=[[cid] for cid in candidates_sentences_ids['main']])
        print("Loss = %f" % loss)
        return loss

    def answersummaries(self, questions_and_candidates, beamwidth=0):
        if beamwidth > 0:
            print("Beam width is", beamwidth)
            return answersummaries(questions_and_candidates, self.extractfeatures, self.nnc.predict, beamwidth)
        else:
            return answersummaries(questions_and_candidates, self.extractfeatures, self.nnc.predict)

    def answersummary(self, question, candidates_sentences,
                      n=3, qindex=None):
        """Return a summary that answers the question

        qindex is not used but needed for compatibility with oracle"""
        return self.answersummaries((question, candidates_sentences, n))

def evaluate_one(di, dataset, testindices, nanswers, rougepath):
    """Evaluate one question"""
    if di not in testindices:
        return None
    question = dataset[di]['body']
    if 'snippets' not in dataset[di].keys():
        return None
    candidates = [(sent, sentid) for (pubmedid, sentid, sent) in yield_candidate_text(dataset[di])]
    if len(candidates) == 0:
        # print("Warning: No text to summarise; ignoring this text")
        return None

    if type(nanswers) == dict:
        n = nanswers[dataset[di]['type']]
    else:
        n = nanswers
    rouge_text = """<EVAL ID="%i">
 <MODEL-ROOT>
 %s/models
 </MODEL-ROOT>
 <PEER-ROOT>
 %s/summaries
 </PEER-ROOT>
 <INPUT-FORMAT TYPE="SEE">
 </INPUT-FORMAT>
""" % (di, rougepath, rougepath)
    rouge_text += """ <PEERS>
  <P ID="A">summary%i.txt</P>
 </PEERS>
 <MODELS>
""" % (di)

    if type(dataset[di]['ideal_answer']) == list:
        ideal_answers = dataset[di]['ideal_answer']
    else:
        ideal_answers = [dataset[di]['ideal_answer']]

    for j in range(len(ideal_answers)):
        rouge_text += '  <M ID="%i">ideal_answer%i_%i.txt</M>\n' % (j,di,j)
        with codecs.open(rougepath + '/models/ideal_answer%i_%i.txt' % (di,j),
                         'w', 'utf-8') as fout:
            a = '<html>\n<head>\n<title>system</title>\n</head>\n<body bgcolor="white">\n<a name="1">[1]</a> <a href="#1" id=1>{0}</body>\n</html>'.format(ideal_answers[j])
            fout.write(a+'\n')
    rouge_text += """ </MODELS>
</EVAL>
"""
    target = {'id': dataset[di]['id'],
              'ideal_answer': ideal_answers,
              'exact_answer': ""}
    return rouge_text, di, (question, candidates, n), target

def evaluate(classificationClassInstance, rougeFilename="rouge.xml", nanswers=3,
             tmppath='', load_models=False, small_data=False, fold=0):
    """Evaluate a classification-based summariser

    nanswers is the number of answers. If it is a dictionary, then the keys indicate the question type, e.g.
    nanswers = {"summary": 6,
                "factoid": 2,
                "yesno": 2,
                "list": 3}
"""
    if tmppath == '':
        modelspath = 'saved_models_Similarities'
        rougepath = '../rouge'
        crossvalidationpath = 'crossvalidation'
    else:
        modelspath = tmppath + '/saved_models'
        rougepath = tmppath + '/rouge'
        crossvalidationpath = tmppath + '/crossvalidation'
        rougeFilename = rougepath + "/" + rougeFilename
        if not os.path.exists(rougepath):
            os.mkdir(rougepath)
        for f in glob.glob(rougepath + '/*'):
            if os.path.isfile(f):
                os.remove(f)
            elif os.path.isdir(f):
                shutil.rmtree(f)
            else:
                print("Warning: %f is neither a file nor a directory" % (f))
        os.mkdir(rougepath + '/models')
        os.mkdir(rougepath + '/summaries')
        if not os.path.exists(crossvalidationpath):
            os.mkdir(crossvalidationpath)

    dataset = classificationClassInstance.data
    indices = [i for i in range(len(dataset))
               #if dataset[i]['type'] == 'summary'
               #if dataset[i]['type'] == 'factoid'
               #if dataset[i]['type'] == 'yesno'
               #if dataset[i]['type'] == 'list'
               ]
    if small_data:
        indices = indices[:100]

    random.seed(1234)
    random.shuffle(indices)

    rouge_results = []
    rouge_results_P = []
    rouge_results_R = []
    the_fold = 0
    kf = KFold(n_splits=10)
    for (traini, testi) in kf.split(indices):
        the_fold += 1

        if fold > 0 and the_fold != fold:
            continue

        if small_data and the_fold > 2:
           break

        print("Cross-validation Fold %i" % the_fold)
        trainindices = [indices[i] for i in traini]
        testindices = [indices[i] for i in testi]

        save_test_predictions = crossvalidationpath + "/test_results_%i.csv" % the_fold
        (trainloss,testloss) = classificationClassInstance.train(trainindices,
                                                             testindices,
                                                             foldnumber=the_fold,
                                                             restore_model=load_models,
                                                             savepath="%s/saved_model_%i" % (modelspath, the_fold),
                                                             save_test_predictions=save_test_predictions)

        for f in glob.glob(rougepath+'/models/*')+glob.glob(rougepath+'/summaries/*'):
            os.remove(f)

        with open(rougeFilename,'w') as frouge:
           print("Collecting evaluation results")
           frouge.write('<ROUGE-EVAL version="1.0">\n')
           with Pool() as pool:
               evaluation_data = \
                  pool.map(partial(evaluate_one,
                                   dataset=dataset,
                                   testindices=testindices,
                                   nanswers=nanswers,
                                   rougepath=rougepath),
                           range(len(dataset)))

           summaries = classificationClassInstance.answersummaries([e[2] for e in evaluation_data if e != None])

           eval_test_system = []
           eval_test_target = []
           for data_item in evaluation_data:
               if data_item == None:
                   continue
               rouge_item, di, system_item, target_item = data_item
               summary = summaries.pop(0)
               #print(di)
               with codecs.open(rougepath+'/summaries/summary%i.txt' % (di),
                               'w', 'utf-8') as fout:
                   a = '<html>\n<head>\n<title>system</title>\n</head>\n<body bgcolor="white">\n<a name="1">[1]</a> <a href="#1" id=1>{0}</body>\n</html>'.format(" ".join(summary))
                   fout.write(a+'\n')
                   # fout.write('\n'.join([s for s in summary])+'\n')

               frouge.write(rouge_item)
               system_item = {'id': dataset[di]['id'],
                              'ideal_answer': " ".join(summary),
                              'exact_answer': ""}

               eval_test_system.append(system_item)
               eval_test_target.append(target_item)

           assert len(summaries) == 0

           frouge.write('</ROUGE-EVAL>\n')

        json_summaries_file = crossvalidationpath + "/crossvalidation_%i_summaries.json" % the_fold
        print("Saving summaries in file %s" % json_summaries_file)
        with open(json_summaries_file,'w') as fcv:
            fcv.write(json.dumps({'questions': eval_test_system}, indent=2))
        json_gold_file = crossvalidationpath + "/crossvalidation_%i_gold.json" % the_fold
        print("Saving gold data in file %s" % json_gold_file)
        with open(json_gold_file,'w') as fcv:
            fcv.write(json.dumps({'questions': eval_test_target}, indent=2))

        print("Calling ROUGE", rougeFilename)
        ROUGE_CMD = 'perl ../rouge/RELEASE-1.5.5/ROUGE-1.5.5.pl -e ' \
            + '../rouge/RELEASE-1.5.5/data -c 95 -2 4 -u -x -n 4 -a ' \
            + rougeFilename
        stream = Popen(ROUGE_CMD, shell=True, stdout=PIPE).stdout
        lines = stream.readlines()
        stream.close()
        for l in lines:
            print(l.decode('ascii').strip())
        print()

        F = {'N-1':float(lines[3].split()[3]),
             'N-2':float(lines[7].split()[3]),
             'L':float(lines[11].split()[3]),
             'S4':float(lines[15].split()[3]),
             'SU4':float(lines[19].split()[3]),
             'trainloss':trainloss,
             'testloss':testloss}
        P = {'N-1':float(lines[2].split()[3]),
             'N-2':float(lines[6].split()[3]),
             'L':float(lines[10].split()[3]),
             'S4':float(lines[14].split()[3]),
             'SU4':float(lines[18].split()[3]),
             'trainloss':trainloss,
             'testloss':testloss}
        R = {'N-1':float(lines[1].split()[3]),
             'N-2':float(lines[5].split()[3]),
             'L':float(lines[9].split()[3]),
             'S4':float(lines[15].split()[3]),
             'SU4':float(lines[17].split()[3]),
             'trainloss':trainloss,
             'testloss':testloss}
        rouge_results.append(F)
        rouge_results_P.append(P)
        rouge_results_R.append(R)

        print("F N-2: %1.5f SU4: %1.5f TrainLoss: %1.5f TestLoss: %1.5f" % (
               F['N-2'], F['SU4'], F['trainloss'], F['testloss']
        ))
        print("P N-2: %1.5f SU4: %1.5f TrainLoss: %1.5f TestLoss: %1.5f" % (
               P['N-2'], P['SU4'], P['trainloss'], P['testloss']
        ))
        print("R N-2: %1.5f SU4: %1.5f TrainLoss: %1.5f TestLoss: %1.5f" % (
               R['N-2'], R['SU4'], R['trainloss'], R['testloss']
        ))


    print("%5s %7s %7s %7s %7s" % ('', 'N-2', 'SU4', 'TrainLoss', 'TestLoss'))
    for i in range(len(rouge_results)):
        print("%5i %1.5f %1.5f %1.5f %1.5f" % (i+1,rouge_results[i]['N-2'],rouge_results[i]['SU4'],
                                       rouge_results[i]['trainloss'],rouge_results[i]['testloss']))
    mean_N2 = np.average([rouge_results[i]['N-2']
                          for i in range(len(rouge_results))])
    mean_SU4 = np.average([rouge_results[i]['SU4']
                           for i in range(len(rouge_results))])
    mean_N2_P = np.average([rouge_results_P[i]['N-2']
                          for i in range(len(rouge_results_P))])
    mean_SU4_P = np.average([rouge_results_P[i]['SU4']
                           for i in range(len(rouge_results_P))])
    mean_N2_R = np.average([rouge_results_R[i]['N-2']
                          for i in range(len(rouge_results_R))])
    mean_SU4_R = np.average([rouge_results_R[i]['SU4']
                           for i in range(len(rouge_results_R))])
    mean_Trainloss = np.average([rouge_results[i]['trainloss']
                                 for i in range(len(rouge_results))])
    mean_Testloss = np.average([rouge_results[i]['testloss']
                                for i in range(len(rouge_results))])
    print("%5s %1.5f %1.5f %1.5f %1.5f" % ("mean",mean_N2,mean_SU4,mean_Trainloss,mean_Testloss))
    stdev_N2 = np.std([rouge_results[i]['N-2']
                       for i in range(len(rouge_results))])
    stdev_SU4 = np.std([rouge_results[i]['SU4']
                        for i in range(len(rouge_results))])
    stdev_N2_P = np.std([rouge_results_P[i]['N-2']
                       for i in range(len(rouge_results_P))])
    stdev_SU4_P = np.std([rouge_results_P[i]['SU4']
                        for i in range(len(rouge_results_P))])
    stdev_N2_R = np.std([rouge_results_R[i]['N-2']
                       for i in range(len(rouge_results_R))])
    stdev_SU4_R = np.std([rouge_results_R[i]['SU4']
                        for i in range(len(rouge_results_R))])
    stdev_Trainloss = np.std([rouge_results[i]['trainloss']
                              for i in range(len(rouge_results))])
    stdev_Testloss = np.std([rouge_results[i]['testloss']
                             for i in range(len(rouge_results))])
    print("%5s %1.5f %1.5f %1.5f %1.5f" % ("stdev",stdev_N2,stdev_SU4,stdev_Trainloss,stdev_Testloss))
    print()
    return mean_SU4, stdev_SU4, mean_SU4_P, stdev_SU4_P, mean_SU4_R, stdev_SU4_R, mean_Testloss, stdev_Testloss

if __name__ == "__main__":
    import doctest
    doctest.testmod()

    import sys
    #bioasq_train()
    #print ("SAVED MODEL - NOW TRY RUNNING")
    #bioasq_run()
    #sys.exit()

    import argparse
    import time
    import socket
    start_time = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--nb_epoch', type=int, default=3,
                        help="Number of training epochs")
    parser.add_argument('-v', '--verbose', type=int, default=1,
                        help="Verbosity level")
    parser.add_argument('-t', '--classification_type',
                        choices=("BasicNN", # "CNN", "LSTM", "Bi-LSTM",
                                 "Similarities", "CNNSimilarities", "LSTMSimilarities",
                                 "SimilaritiesYu", "CNNSimilaritiesYu", "LSTMSimilaritiesYu",
                                 "SimilaritiesEuc", "CNNSimilaritiesEuc", "LSTMSimilaritiesEuc"),
                        default="Similarities",
                        help="Type of classification")
    parser.add_argument('-S', '--small', action="store_true",
                        help='Run on a small subset of the data')
    parser.add_argument('-l', '--load', action="store_true",
                        help='load pre-trained model')
    parser.add_argument('-m', '--embeddings', action="store_true",
                        help="Use pre-trained embeddings")
    parser.add_argument('-d', '--hidden_layer', type=int, default=50,
                        help="Size of the hidden layer (0 if there is no hidden layer)")
    parser.add_argument('-r', '--dropout', type=float, default=0.9,
                        help="Keep probability for the dropout layers")
    parser.add_argument('-a', '--tmppath', default='',
                        help="Path for temporary data and files")
    parser.add_argument('-n', '--rouge_labels', default=False,action='store_true',
                        help="Use raw rouge scores as labels. If not specified labels are converted to True/False categories")
    parser.add_argument('-g', '--regression', default=False,action='store_true',
                        help="Use regression model (linear activation) instead of classification model (sigmoid activation)")
    parser.add_argument('-w', '--reinforce', default=False,action='store_true',
                        help="Use reinforce model")
    parser.add_argument('-s', '--batch_size', type=int, default=4096,
                        help="Batch size for gradient descent")
    parser.add_argument('-c', '--svd_components', type=int, default=100,
                        help="Depth of the LSTM stack")
    parser.add_argument("-f", "--fold", type=int, default=0,
                        help="Use only the specified fold (0 for all folds)")

    args = parser.parse_args()

    if args.tmppath != '':
        nnc.DB = "%s/%s" % (args.tmppath, nnc.DB)

    print("rouge_labels: %s" % args.rouge_labels)
    classifier = Classification('BioASQ-training7b.json',
                           'rouge_7b.csv',
                           rouge_labels=args.rouge_labels,
                           regression=args.regression,
                           reinforce=args.reinforce,
                           nb_epoch=args.nb_epoch,
                           verbose=args.verbose,
                           classification_type=args.classification_type,
                           embeddings=args.embeddings,
                           hidden_layer=args.hidden_layer,
                           dropout=args.dropout,
                           batch_size=args.batch_size)

    print("%s with epochs=%i and batch size=%i" % (classifier.nnc.name(), 
                                                   args.nb_epoch,
                                                   args.batch_size))


    mean_SU4, stdev_SU4, mean_SU4_P, stdev_SU4_P, mean_SU4_R, stdev_SU4_R, mean_Testloss, stdev_Testloss = \
                              evaluate(classifier,
                                       nanswers={"summary": 6,
                                                 "factoid": 2,
                                                 "yesno": 2,
                                                 "list": 3},
                                       tmppath=args.tmppath,
                                       load_models=args.load,
                                       small_data=args.small,
                                       fold = args.fold)
    end_time = time.time()
    elapsed = time.strftime("%X", time.gmtime(end_time - start_time))
    print("Time elapsed: %s" % (elapsed))
    print("| Type | Epochs | Dropout | meanSU4 | stdevSU4 | meanSU4_P | stdevSU4_P | meanSU4_R | stdevSU4_R | meanTestLoss | stdevTestLoss | Time | Hostname |")
    print("| %s | %i | %f | %f | %f | %f | %f | %f | %f | %f | %f | %s | %s |" % \
               (classifier.nnc.name(),
                args.nb_epoch,
                args.dropout,
                mean_SU4,
                stdev_SU4,
                mean_SU4_P,
                stdev_SU4_P,
                mean_SU4_R,
                stdev_SU4_R,
                mean_Testloss,
                stdev_Testloss,
                elapsed,
                socket.gethostname()))

